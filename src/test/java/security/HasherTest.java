package security;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class HasherTest {
    private static Hasher hasher;

    @BeforeClass
    public static void init() throws HashingAlgorithmException {
        hasher = new Hasher();
    }

    @Test
    public void getHash1() {
        try {
            String password = "password";
            String salt = "salt";
            Assert.assertEquals("Hashes for the same password and salt must equals", hasher.getHash(password, salt), hasher.getHash(password, salt));
        } catch (HashingAlgorithmException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void getHash2() {
        try {
            String password = "password";
            String salt1 = "first";
            String salt2 = "second";
            Assert.assertFalse("Hashes for the same password and different salt can't be equals",
                    hasher.getHash(password, salt1).equals(hasher.getHash(password, salt2)));
        } catch (HashingAlgorithmException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
    }


    @Test
    public void createHashAndSalt1() {
        String password = "password";
        try {
            HashSaltPair pair1 = hasher.createHashAndSalt(password);
            HashSaltPair pair2 = hasher.createHashAndSalt(password);
            Assert.assertFalse("Hash and salt pairs can't be equals for the same password", pair1.equals(pair2));
        } catch (HashingAlgorithmException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void createHashAndSalt2() {
        String password = "password";
        try {
            HashSaltPair pair1 = hasher.createHashAndSalt(password);
            HashSaltPair pair2 = hasher.createHashAndSalt(password);
            Assert.assertFalse("Hashes can't be equals for the same password", pair1.getHash().equals(pair2.getHash()));
        } catch (HashingAlgorithmException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void createHashAndSalt3() {
        String password = "password";
        try {
            HashSaltPair pair1 = hasher.createHashAndSalt(password);
            HashSaltPair pair2 = hasher.createHashAndSalt(password);
            Assert.assertFalse("Salts can't be equals for the same password", pair1.getSalt().equals(pair2.getSalt()));
        } catch (HashingAlgorithmException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }
    }
}