package application.resultcomparator;

import application.rally.Pilot;
import application.rally.RaceTime;
import application.rally.Result;
import application.rally.Team;
import org.junit.Assert;
import org.junit.Test;


public class ResultTimeComparatorTest {

    private static Pilot pilot1 = new Pilot("Name1", 1);
    private static Pilot pilot2 = new Pilot("Name2", 2);

    private static Team team1 = new Team(1, pilot1, pilot2, 1, "first car");
    private static Team team2 = new Team(2, pilot1, pilot2, 2, "second car");

    @Test
    public void testCompare1() {
        RaceTime raceTime1 = new RaceTime(true, 1, 1, 1, 1);
        RaceTime raceTime2 = new RaceTime(true, 2, 2, 2, 2);
        Result result1 = new Result(1, raceTime1, team1);
        Result result2 = new Result(1, raceTime2, team2);
        ResultTimeComparator comparator = new ResultTimeComparator();
        Assert.assertEquals(-1, comparator.compare(result1, result2));
    }

    @Test
    public void testCompare2() {
        RaceTime raceTime1 = new RaceTime(true, 1, 1, 1, 1);
        RaceTime raceTime2 = new RaceTime(false, 0, 0, 0, 2);
        Result result1 = new Result(1, raceTime1, team1);
        Result result2 = new Result(1, raceTime2, team2);
        ResultTimeComparator comparator = new ResultTimeComparator();
        Assert.assertEquals(-1, comparator.compare(result1, result2));
    }

    @Test
    public void testCompare3() {
        RaceTime raceTime1 = new RaceTime(true, 5, 12, 33, 17);
        RaceTime raceTime2 = new RaceTime(true, 0, 0, 0, 2);
        Result result1 = new Result(1, raceTime1, team1);
        Result result2 = new Result(1, raceTime2, team2);
        ResultTimeComparator comparator = new ResultTimeComparator();
        Assert.assertEquals(1, comparator.compare(result1, result2));
    }

    @Test
    public void testCompare4() {
        RaceTime raceTime1 = new RaceTime(false, 5, 12, 33, 17);
        RaceTime raceTime2 = new RaceTime(true, 10, 0, 0, 2);
        Result result1 = new Result(1, raceTime1, team1);
        Result result2 = new Result(1, raceTime2, team2);
        ResultTimeComparator comparator = new ResultTimeComparator();
        Assert.assertEquals(1, comparator.compare(result1, result2));
    }
}