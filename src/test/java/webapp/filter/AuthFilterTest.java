package webapp.filter;

import application.user.User;
import application.user.UserRole;
import data.userdao.UserDao;
import data.userdao.exception.GetUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;
import webapp.AttributeName;
import webapp.state.AdminState;
import webapp.state.State;
import webapp.state.UnknownState;
import webapp.state.UserState;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthFilterTest {

    @Before
    public void setUp() throws Exception {
    }

    /**
     * User data contained in the session.
     * {@code getClientState} must return
     * {@code UserState} without any queries to the DAO.
     */
    @Test
    public void getClientState1() {
        String login = "user";
        String password = "userpassword";

        HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
        Mockito.when(requestMock.getParameter(AttributeName.LOGIN)).thenReturn(login);
        Mockito.when(requestMock.getParameter(AttributeName.PASSWORD)).thenReturn(password);

        HttpServletResponse responseMock = Mockito.mock(HttpServletResponse.class);

        HttpSession sessionMock = Mockito.mock(HttpSession.class);
        Mockito.when(sessionMock.getAttribute(AttributeName.LOGIN)).thenReturn(login);
        Mockito.when(sessionMock.getAttribute(AttributeName.PASSWORD)).thenReturn(password);
        Mockito.when(sessionMock.getAttribute(AttributeName.ROLE)).thenReturn(UserRole.USER);

        UserDao daoMock = Mockito.mock(UserDao.class);

        State state = new AuthFilter().getClientState(requestMock, responseMock, daoMock, sessionMock);
//        Mockito.verify(daoMock, Mockito.never()).getById(Mockito.any());
        Mockito.verifyNoMoreInteractions(daoMock);
        Assert.assertTrue(state instanceof UserState);
    }

    /**
     * Admin data contained in the session.
     * {@code getClientState} must return
     * {@code AdminState} without any queries to the DAO.
     */
    @Test
    public void getClientState2() {
        String login = "admin";
        String password = "adminpassword";

        HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
        Mockito.when(requestMock.getParameter(AttributeName.LOGIN)).thenReturn(login);
        Mockito.when(requestMock.getParameter(AttributeName.PASSWORD)).thenReturn(password);

        HttpServletResponse responseMock = Mockito.mock(HttpServletResponse.class);

        HttpSession sessionMock = Mockito.mock(HttpSession.class);
        Mockito.when(sessionMock.getAttribute(AttributeName.LOGIN)).thenReturn(login);
        Mockito.when(sessionMock.getAttribute(AttributeName.PASSWORD)).thenReturn(password);
        Mockito.when(sessionMock.getAttribute(AttributeName.ROLE)).thenReturn(UserRole.ADMIN);

        UserDao daoMock = Mockito.mock(UserDao.class);

        State state = new AuthFilter().getClientState(requestMock, responseMock, daoMock, sessionMock);

        Mockito.verifyNoMoreInteractions(daoMock);
        Assert.assertTrue(state instanceof AdminState);
    }

    /**
     * Session is clear.
     * {@code getClientState} must make query to the DAO and sets 4 session's attributes.
     */
    @Test
    public void getClientState3() throws NoSuchUserException, GetUserDaoErrorException {
        String login = "user";
        String password = "userpassword";

        HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
        Mockito.when(requestMock.getParameter(AttributeName.LOGIN)).thenReturn(login);
        Mockito.when(requestMock.getParameter(AttributeName.PASSWORD)).thenReturn(password);

        HttpServletResponse responseMock = Mockito.mock(HttpServletResponse.class);

        HttpSession sessionMock = Mockito.mock(HttpSession.class);
        Mockito.when(sessionMock.getAttribute(AttributeName.LOGIN)).thenReturn(null);
        Mockito.when(sessionMock.getAttribute(AttributeName.PASSWORD)).thenReturn(null);
        Mockito.when(sessionMock.getAttribute(AttributeName.ROLE)).thenReturn(null);

        User user = new User(1, login, "hash", UserRole.USER, "salt", null);
        UserDao daoMock = Mockito.mock(UserDao.class);
        Mockito.when(daoMock.getByLoginAndPassword(login, password)).thenReturn(user);

        State state = new AuthFilter().getClientState(requestMock, responseMock, daoMock, sessionMock);

        Mockito.verify(daoMock).getByLoginAndPassword(login, password);
        Mockito.verify(sessionMock, Mockito.times(4)).setAttribute(Mockito.any(), Mockito.any());
        Assert.assertTrue(state instanceof UserState);
    }


    /**
     * Session is clear.
     * Request contains wrong password.
     * {@code getClientState} must make query to the DAO and return {@code UnknownState}
     * and leave the session untouched.
     */
    @Test
    public void getClientState4() throws NoSuchUserException, GetUserDaoErrorException {
        String login = "user";
        String password = "wrongpassword";

        HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
        Mockito.when(requestMock.getParameter(AttributeName.LOGIN)).thenReturn(login);
        Mockito.when(requestMock.getParameter(AttributeName.PASSWORD)).thenReturn(password);

        HttpServletResponse responseMock = Mockito.mock(HttpServletResponse.class);

        HttpSession sessionMock = Mockito.mock(HttpSession.class);
        Mockito.when(sessionMock.getAttribute(AttributeName.LOGIN)).thenReturn(null);
        Mockito.when(sessionMock.getAttribute(AttributeName.PASSWORD)).thenReturn(null);
        Mockito.when(sessionMock.getAttribute(AttributeName.ROLE)).thenReturn(null);

        UserDao daoMock = Mockito.mock(UserDao.class);
        Mockito.when(daoMock.getByLoginAndPassword(login, password)).thenThrow(NoSuchUserException.class);

        State state = new AuthFilter().getClientState(requestMock, responseMock, daoMock, sessionMock);

        Mockito.verify(daoMock).getByLoginAndPassword(login, password);
        Mockito.verify(sessionMock, Mockito.never()).setAttribute(Mockito.any(), Mockito.any());
        Assert.assertTrue(state instanceof UnknownState);
    }


}