<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="ajax.js"></script>
    <link href="css/common.css" rel="stylesheet">
    <link href="css/team_creation.css" rel="stylesheet">
    <fmt:requestEncoding value="utf-8"/>
    <fmt:setBundle basename="ResourceBundle"/>
    <title><fmt:message key="create_team"/></title>
</head>
<body>

<div>
    <header>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="get">
                <button class="header-button" type="submit" value="back">
                    <fmt:message key="main_page"/>
                </button>
            </form>
        </div>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="post">
                <button class="header-button" type="submit" name="page" value="COMPETITION_LIST">
                    <fmt:message key="competitions"/>
                </button>
            </form>
        </div>

        <c:if test="${sessionScope.login eq null}">
            <div class="header-button-box">
                <form action="#LogInModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="log_in"/>
                    </button>
                </form>
            </div>
            <div class="header-button-box">
                <form action="#SignUpModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="sign_up"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login eq null}">
            <div id="LogInModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="log_in"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">

                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="login" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="password" pattern="[\w\d]{1,}"><br><br>
                                    <button class="designed_button" type="submit" name="page" value="CABINET">
                                        <fmt:message key="log_in"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="SignUpModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="sign_up"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">
                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="newlogin" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="newpassword" pattern="[\w\d]{1,}"><br><br>
                                        <%--                                    <input type="submit" hidden name="page" value="REGISTRATION">--%>
                                    <button class="designed_button" type="submit" name="page" value="REGISTRATION">
                                        <fmt:message key="sign_up"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </c:if>


        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                        <%--                    <input type="submit" name="page" value="CABINET"/>--%>
                    <button class="header-button" type="submit" name="page" value="CABINET">
                        <fmt:message key="cabinet"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                    <button class="header-button" type="submit" name="page" onclick="update()" value="LOGOUT">
                        <fmt:message key="logout"/>
                    </button>
                </form>
            </div>
        </c:if>
        <script>
            function update() {
                window.location.reload();
            }
        </script>
    </header>
</div>


<div class="main-content">
    <div class="team-creation">
        <label for="new_driver"></label>
        <input class="text-field" id="new_driver" required type="text" name="new_driver" pattern="[a-zA-ZА-Яа-я\s]{2,100}"
               placeholder="<fmt:message key="pilot_name"/>">
        <br>
        <label for="new_codriver"></label>
        <input class="text-field" id="new_codriver" required type="text" name="new_codriver" pattern="[a-zA-ZА-Яа-я\s]{2,100}"
               placeholder="<fmt:message key="codriver_name"/>">
        <br>
        <label for="new_car"></label>
        <input class="text-field" id="new_car" required type="text" name="new_car"
               placeholder="<fmt:message key="your_car"/>">
        <br>
        <label for="new_car_number"><fmt:message key="your_car_number"/></label>
        <input class="text-field" id="new_car_number" required type="number" min="0" name="new_car_number">
        <br>
    </div>
    <button id="confirm-button" class="designed_button" onclick="send()"><fmt:message key="save"/></button>

    <div id="new_team_result">

    </div>


    <script>
        function validate(object, regex) {
            if (object.value === "") {
                return false;
            }
            if (!regex.test(object.value)) {
                object.focus();
                return false;
            }

            // validation was successful
            return true;
        }

        function send() {
            var driver = document.getElementById("new_driver");
            var codriver = document.getElementById("new_codriver");
            var car = document.getElementById("new_car");
            var carNumber = document.getElementById("new_car_number");

            document.getElementById("new_team_result").innerHTML = '<fmt:message key="processing"/>...';
            if (validate(driver, /^[a-zA-ZА-Яа-я\s]{2,100}$/)
                && validate(codriver, /^[a-zA-ZА-Яа-я\s]{2,100}$/)
                && validate(car, /^[a-zA-ZА-Яа-я0-9\s]{2,100}$/)
                && validate(carNumber, /^[0-9]{1,5}$/)) {

                $.ajax({
                    type: "POST",
                    url: "<c:url value="/servlet"/>",
                    data: "page=CREATE_TEAM&new_driver=" + driver.value + "&new_codriver=" + codriver.value + "&new_car=" + car.value + "&new_car_number=" + carNumber.value,
                    success: function () {
                        document.getElementById("new_team_result").innerHTML = '<fmt:message key="saved"/>';
                    },
                    error: function () {
                        document.getElementById("new_team_result").innerHTML = '<fmt:message key="error_try_again_later"/>';
                    }
                });
            } else {
                document.getElementById("new_team_result").innerHTML = '<fmt:message key="wrong_format"/>...';
            }
        }
    </script>
</div>
</body>
</html>
