<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:requestEncoding value="utf-8"/>
    <fmt:setBundle basename="ResourceBundle"/>
    <title><fmt:message key="open_events"/></title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="ajax.js"></script>
    <link href="css/common.css" rel="stylesheet">
</head>
<body>

<div>
    <header>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="get">
                <button class="header-button" type="submit" value="back">
                    <fmt:message key="main_page"/>
                </button>
            </form>
        </div>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="post">
                <button class="header-button" type="submit" name="page" value="COMPETITION_LIST">
                    <fmt:message key="competitions"/>
                </button>
            </form>
        </div>

        <c:if test="${sessionScope.login eq null}">
            <div class="header-button-box">
                <form action="#LogInModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="log_in"/>
                    </button>
                </form>
            </div>
            <div class="header-button-box">
                <form action="#SignUpModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="sign_up"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login eq null}">
            <div id="LogInModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="log_in"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">

                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="login" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="password" pattern="[\w\d]{1,}"><br><br>
                                    <button class="designed_button" type="submit" name="page" value="CABINET">
                                        <fmt:message key="log_in"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="SignUpModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="sign_up"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">
                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="newlogin" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="newpassword" pattern="[\w\d]{1,}"><br><br>
                                        <%--                                    <input type="submit" hidden name="page" value="REGISTRATION">--%>
                                    <button class="designed_button" type="submit" name="page" value="REGISTRATION">
                                        <fmt:message key="sign_up"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </c:if>


        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                        <%--                    <input type="submit" name="page" value="CABINET"/>--%>
                    <button class="header-button" type="submit" name="page" value="CABINET">
                        <fmt:message key="cabinet"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                    <button class="header-button" type="submit" name="page" onclick="update()" value="LOGOUT">
                        <fmt:message key="logout"/>
                    </button>
                </form>
            </div>
        </c:if>
        <script>
            function update() {
                window.location.reload();
            }
        </script>
    </header>
</div>

<div class="main-content">
    <div>
        <fmt:message key="open_events"/>
    </div>

    <table>
        <c:forEach var="event" items="${requestScope.events}">
            <tr>
                <td>
                        ${fn:escapeXml(event.name)}
                </td>
                <td>
                        ${fn:escapeXml(event.country)}
                </td>
                <td>
                        ${fn:escapeXml(event.description)}
                </td>
                <td>

                    <c:set var="flag" scope="page">false</c:set>
                    <c:forEach var="eventWithUser" items="${requestScope.events_with_this_user}">
                        <c:if test="${eventWithUser.idEvent eq event.idEvent}">
                            <c:set var="flag">true</c:set>

                        </c:if>
                    </c:forEach>

                    <label> <fmt:message key="involved"/>
                        <input type="checkbox"
                               <c:if test="${flag eq true}">checked</c:if> id="checkbox${fn:escapeXml(event.idEvent)}">
                    </label>

                    <c:remove var="flag"/>
                </td>
            </tr>
        </c:forEach>
    </table>

    <button class="designed_button" onclick="send()"><fmt:message key="save"/></button>
    <div id="handling_result"></div>

    <script>
        function send() {
            document.getElementById("handling_result").innerHTML = '<fmt:message key="processing"/>...';
            let arr = [];
            <c:forEach var="event" items="${requestScope.events}">
            {
                let currectEvent = {
                    id: ${event.idEvent},
                    value: document.getElementById("checkbox${event.idEvent}").checked
                };
                arr.push(currectEvent);
            }
            </c:forEach>
            $.ajax({
                type: "POST",
                url: "<c:url value="/servlet"/>",
                data: "page=PARTICIPATE&participate_data=" + JSON.stringify(arr),
                success: function () {
                    document.getElementById("handling_result").innerHTML = '<fmt:message key="saved"/>'
                },
                error: function () {
                    document.getElementById("handling_result").innerHTML = '<fmt:message key="error_try_again_later"/>'

                }
            });
        }
    </script>

</div>
</body>
</html>
