<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:requestEncoding value="utf-8"/>
    <fmt:setBundle basename="ResourceBundle"/>
    <title><fmt:message key="add_result"/></title>
    <link href="css/result_edit.css" rel="stylesheet">
    <link href="css/common.css" rel="stylesheet">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="ajax.js"></script>
</head>
<body>

<div class="control-panel">
    <button class="add-result-button designed_button" onclick="addResult()">
        <fmt:message key="add_result"/>
    </button>
    <form action="<c:url value="/servlet"/>" method="post">
        <button class="cancel-button designed_button" type="submit" name="page" value="CABINET">
            <fmt:message key="cancel"/>
        </button>
    </form>
    <button class="save-button designed_button" onclick="save()">
        <fmt:message key="save"/>
    </button>

    <div id="status"></div>
</div>

<script>
    function validate(object, regex) {
        if (object.value === "") {
            return false;
        }
        if (!regex.test(object.value)) {
            object.focus();
            return false;
        }

        // validation was successful
        return true;
    }

    function save() {
        if (window.counter === undefined) {
            window.counter = 1;
        }
        for (var i = 0; i < window.counter; i++) {
            var result = document.getElementById("result" + i);
            if (result === undefined || result === null) {
                continue;
            }
            if (!validate(result.childNodes.item(1).childNodes.item(1), /^\d{1,6}$/)
                || !validate(result.childNodes.item(3).childNodes.item(1), /^\d{1,6}$/)
                || !validate(result.childNodes.item(5).childNodes.item(1), /^\d{1,2}[-.,]\d{1,2}[-,.]\d{1,2}[-,.]\d$/)) {

                document.getElementById("status").innerHTML = '<fmt:message key="wrong_format"/>';
                return;
            }
        }

        var resultArray = collectResultData();
        var jsonData = JSON.stringify(resultArray);
        $.ajax({
            type: "POST",
            url: "<c:url value="/servlet"/>",
            data: "page=EDIT_RESULT&data=" + jsonData,
            success: function () {
                document.getElementById("status").innerHTML = '<fmt:message key="saved"/>';
            },
            error: function () {
                document.getElementById("status").innerHTML = '<fmt:message key="error_try_again_later"/>';
            }
        })

    }

    function collectResultData() {
        if (window.counter === undefined) {
            window.counter = 1;
        }
        var resultArray = [];

        for (var i = 0; i < window.counter; i++) {
            var result = document.getElementById("result" + i);
            if (result === undefined || result === null) {
                continue;
            }
            var eventId = result.childNodes.item(1).childNodes.item(1).value;
            var teamId = result.childNodes.item(3).childNodes.item(1).value;
            var time = result.childNodes.item(5).childNodes.item(1).value;
            var isFinished = result.childNodes.item(7).childNodes.item(1).checked;
            if (!isFinished)
                time = 0;

            let jsonResult = {
                eventId: eventId,
                resultTime: time,
                teamId: teamId
            };
            resultArray.push(jsonResult);
        }
        return resultArray;
    }
</script>

<div class="workspace">
    <div class="result-container" id="result-container">

        <div class="result" id="result0">
            <label>
                <input class="id-event-field text-field" type="text" name="id-event"
                       placeholder="<fmt:message key="event_id"/>">
            </label>
            <label>
                <input class="id-team-field text-field" type="text" name="id-team"
                       placeholder="<fmt:message key="team_id"/>">
            </label>
            <label>
                <input class="result-field text-field" type="text" name="result" placeholder="hh-mm-ss-ms">
            </label>
            <label>
                <input class="is-finished-field" type="checkbox" checked name="is-finished">
                <fmt:message key="finished"/>
            </label>
        </div>

    </div>
</div>


<script>
    function removeResult(object) {
        var id = object.getAttribute("id");
        if (id === "result0")
            return;
        object.parentNode.removeChild(object);
    }

    function addResult() {
        if (typeof (window.counter) == 'undefined') {
            window.counter = 1;
        }
        var container = document.getElementById("result-container");
        var input = document.getElementById("result0");
        var newinput = input.cloneNode(true);
        newinput.setAttribute("id", "result" + window.counter);
        newinput.innerHTML +=
            "<button class=\"remove-result designed_button\" onclick=\"removeResult(this.parentNode)\">\n" +
            '<fmt:message key="delete"/>' +
            "</button>";
        window.counter++;
        container.appendChild(newinput);
    }
</script>

<div class="search_panel">
    <div class="event_search_block">
        <div class="search_box">
            <div class="search_input">
                <label for="event_search">
                    <fmt:message key="search_event_by_name"/>
                </label>
                <input class="text-field" type="text" id="event_search" placeholder="<fmt:message key="event_name"/>">
                <button class="designed_button" onclick="eventSearch()">
                    <fmt:message key="search"/>
                </button>
            </div>
            <table id="event_search_result">
            </table>
        </div>
    </div>
    <div class="team_search_box">
        <div class="search_box">
            <div class="search_input">
                <label for="team_search">
                    <fmt:message key="search_team_by_name"/>
                </label>
                <input class="text-field" type="text" id="team_search"
                       placeholder="<fmt:message key="name_of_one_of_the_pilots"/>">
                <button class="designed_button" onclick="teamSearch()">
                    <fmt:message key="search"/>
                </button>
            </div>
            <table id="team_search_result">
            </table>
        </div>
    </div>
</div>


<script>
    function teamSearch() {
        if (window.jsonTeamData === undefined) {
            window.jsonTeamData = JSON.parse('${requestScope.teams}');
        }

        document.getElementById("team_search_result").innerHTML = '<tr><td><fmt:message key="event_id"/></td><td><fmt:message key="driver"/></td><td><fmt:message key="codriver"/></td></tr>';
        var jsonData = window.jsonTeamData;
        for (var i = 0; i < jsonData.length; i++) {
            var thisTeam = jsonData[i];
            var substr = document.getElementById("team_search").value;
            if (thisTeam.driver.name.indexOf(substr, 0) !== -1 || thisTeam.codriver.name.indexOf(substr, 0) !== -1) {
                document.getElementById("team_search_result").innerHTML += '<tr><td>' + thisTeam.id + '</td><td>' + thisTeam.driver.name + ' </td><td> ' + thisTeam.codriver.name + '</td></tr>';
            }
        }

    }
</script>

<script>
    function eventSearch() {
        if (window.jsonEventData === undefined) {
            window.jsonEventData = JSON.parse('${requestScope.events}');
        }

        let event = {
            open: false,
            eventId: 0,
            eventName: 'none',
            eventDescription: 'none',
            eventCountry: 'none',
            eventIsOpen: false,
            eventStartDate: null,
            eventEndDate: null
        };

        document.getElementById("event_search_result").innerHTML = '<tr><td><fmt:message key="event_id"/></td><td><fmt:message key="name"/></td></tr>';
        <%--var jsonData = JSON.parse('${requestScope.events}');--%>
        var jsonData = window.jsonEventData;

        for (var i = 0; i < jsonData.length; i++) {
            var thisEvent = jsonData[i];
            var substr = document.getElementById("event_search").value;
            if (thisEvent.eventName.indexOf(substr, 0) !== -1) {
                document.getElementById("event_search_result").innerHTML += '<tr><td>' + thisEvent.eventId + '</td><td>' + thisEvent.eventName + '</td></tr>';
                // document.getElementById("event_search_result").innerHTML += thisEvent.eventId + ' ' + thisEvent.eventName + '\n';
            }
        }
    }
</script>


</body>
</html>
