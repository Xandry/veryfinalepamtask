<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:requestEncoding value="utf-8"/>
    <fmt:setBundle basename="ResourceBundle"/>
    <title><fmt:message key="all_users_list"/></title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="ajax.js"></script>
    <link href="css/common.css" rel="stylesheet">
    <link href="css/users_editing.css" rel="stylesheet">
</head>
<body>

<div>
    <header>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="get">
                <button class="header-button" type="submit" value="back">
                    <fmt:message key="main_page"/>
                </button>
            </form>
        </div>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="post">
                <button class="header-button" type="submit" name="page" value="COMPETITION_LIST">
                    <fmt:message key="competitions"/>
                </button>
            </form>
        </div>

        <c:if test="${sessionScope.login eq null}">
            <div class="header-button-box">
                <form action="#LogInModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="log_in"/>
                    </button>
                </form>
            </div>
            <div class="header-button-box">
                <form action="#SignUpModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="sign_up"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login eq null}">
            <div id="LogInModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="log_in"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">

                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="login" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="password" pattern="[\w\d]{1,}"><br><br>
                                    <button class="designed_button" type="submit" name="page" value="CABINET">
                                        <fmt:message key="log_in"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="SignUpModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="sign_up"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">
                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="newlogin" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="newpassword" pattern="[\w\d]{1,}"><br><br>
                                        <%--                                    <input type="submit" hidden name="page" value="REGISTRATION">--%>
                                    <button class="designed_button" type="submit" name="page" value="REGISTRATION">
                                        <fmt:message key="sign_up"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </c:if>


        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                        <%--                    <input type="submit" name="page" value="CABINET"/>--%>
                    <button class="header-button" type="submit" name="page" value="CABINET">
                        <fmt:message key="cabinet"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                    <button class="header-button" type="submit" name="page" onclick="update()" value="LOGOUT">
                        <fmt:message key="logout"/>
                    </button>
                </form>
            </div>
        </c:if>
        <script>
            function update() {
                window.location.reload();
            }
        </script>
    </header>
</div>

<div class="main-content">

    <table>
        <tr>
            <td>ID</td>
            <td><fmt:message key="login"/></td>
            <td><fmt:message key="status"/></td>
            <td><fmt:message key="registered_team"/></td>
        </tr>
        <c:forEach var="user" items="${requestScope.users}">
            <tr>
                <td class="user-cell">
                        ${fn:escapeXml(user.id)}
                </td>
                <td class="user-cell">
                        ${fn:escapeXml(user.login)}
                </td>
                <td class="user-cell">
                        ${fn:escapeXml(user.role)}
                </td>
                <td class="user-cell">
                    <c:if test="${user.idMyTeam ne 0}">
                        ${fn:escapeXml(user.idMyTeam)}
                    </c:if>
                    <c:if test="${user.idMyTeam eq 0}">
                        -
                    </c:if>
                </td>
                <c:if test="${sessionScope.role ne user.role}">
                    <td class="user-cell">
                        <button id="button" class="designed_button" onclick="send(${user.id}, this)"><fmt:message
                                key="delete"/></button>
                    </td>
                </c:if>
            </tr>
        </c:forEach>
    </table>

    <script>
        function send(id, object) {
            object.setAttribute('disabled', true);
            object.innerHTML = '<fmt:message key="deleted"/>';
            $.ajax({
                type: "POST",
                url: "<c:url value="/servlet"/>",
                data: "page=DELETE_USER&id=" + id
            });
        }
    </script>

</div>
</body>
</html>
