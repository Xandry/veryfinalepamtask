<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <fmt:requestEncoding value="utf-8"/>
    <fmt:setBundle basename="ResourceBundle"/>
    <title><fmt:message key="competitions"/></title>
    <link href="css/common.css" rel="stylesheet">
    <link href="css/competitions.css" rel="stylesheet">
</head>
<body>

<div>
    <header>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="get">
                <button class="header-button" type="submit" value="back">
                    <fmt:message key="main_page"/>
                </button>
            </form>
        </div>
        <div class="header-button-box">
            <form action="<c:url value="/servlet"/>" method="post">
                <button class="header-button" type="submit" name="page" value="COMPETITION_LIST">
                    <fmt:message key="competitions"/>
                </button>
            </form>
        </div>

        <c:if test="${sessionScope.login eq null}">
            <div class="header-button-box">
                <form action="#LogInModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="log_in"/>
                    </button>
                </form>
            </div>
            <div class="header-button-box">
                <form action="#SignUpModal">
                    <button class="header-button" type="submit">
                        <fmt:message key="sign_up"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login eq null}">
            <div id="LogInModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="log_in"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">

                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="login" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="password" pattern="[\w\d]{1,}"><br><br>
                                    <button class="designed_button" type="submit" name="page" value="CABINET">
                                        <fmt:message key="log_in"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="SignUpModal" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">
                                <fmt:message key="sign_up"/>
                            </h3>
                            <a href="#close" title="Close" class="close">×</a>
                        </div>
                        <div class="modal-body">

                            <div class="form">
                                <form method="post" action="<c:url value="/servlet"/>">
                                    <input class="text-field" type="text" required
                                           placeholder="<fmt:message key="login"/>"
                                           name="newlogin" pattern="[\w\d]{1,}"><br>
                                    <input class="text-field" type="password" required
                                           placeholder="<fmt:message key="password"/>"
                                           name="newpassword" pattern="[\w\d]{1,}"><br><br>
                                        <%--                                    <input type="submit" hidden name="page" value="REGISTRATION">--%>
                                    <button class="designed_button" type="submit" name="page" value="REGISTRATION">
                                        <fmt:message key="sign_up"/>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </c:if>


        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                        <%--                    <input type="submit" name="page" value="CABINET"/>--%>
                    <button class="header-button" type="submit" name="page" value="CABINET">
                        <fmt:message key="cabinet"/>
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.login ne null}">
            <div class="header-button-box">
                <form action="<c:url value="/servlet"/>" method="post">
                    <button class="header-button" type="submit" name="page" onclick="update()" value="LOGOUT">
                        <fmt:message key="logout"/>
                    </button>
                </form>
            </div>
        </c:if>
        <script>
            function update() {
                window.location.reload();
            }
        </script>
    </header>
</div>


<div class="main-content">
    <div class="events">
        <div class="closed-events">
            <div class="events-header">
                <fmt:message key="close_events_list"/>
            </div>
            <table>
                <c:forEach var="event" items="${requestScope.events}">
                    <c:if test="${event.open eq false}">
                        <tr>
                            <td>
                                <div>
                                    <form action="<c:url value="/servlet"/>" method="post">
                                        <input type="hidden" name="page" value="RESULT"/>
                                        <input type="hidden" name="id" value="${fn:escapeXml(event.idEvent)}"/>
                                        <div class="event-box">

                                            <button class="event-button-box">
                                                <div class="event-name">
                                                        ${fn:escapeXml(event.name)}
                                                </div>
                                                    <%--                                                <br>--%>
                                                <div class="event-country">
                                                        ${fn:escapeXml(event.country)}
                                                </div>
                                                    <%--                                                <br>--%>

                                                <div class="event-dates">
                                                        ${fn:escapeXml(event.startDate)}
                                                    - ${fn:escapeXml(event.endDate)}
                                                </div>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
            </table>
        </div>


        <div class="open-events">
            <div class="events-header">
                <fmt:message key="open_events_list"/>
            </div>
            <table>
                <c:forEach var="event" items="${requestScope.events}">
                    <c:if test="${event.open eq true}">
                        <tr>
                            <td>
                                <div>
                                    <form action="<c:url value="/servlet"/>" method="post">
                                        <input type="hidden" name="page" value="RESULT"/>
                                        <input type="hidden" name="id" value="${fn:escapeXml(event.idEvent)}"/>
                                        <div class="event-box">

                                            <button class="event-button-box">
                                                <div class="event-name">
                                                        ${fn:escapeXml(event.name)}
                                                </div>
                                                    <%--                                                <br>--%>
                                                <div class="event-country">
                                                        ${fn:escapeXml(event.country)}
                                                </div>
                                                    <%--                                                <br>--%>
                                                <div class="event-dates">${fn:escapeXml(event.startDate)}
                                                    - ${fn:escapeXml(event.endDate)}
                                                </div>
                                            </button>
                                        </div>

                                    </form>
                                </div>

                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
</body>
</html>
