<%@ page contentType="text/HTML;charset=UTF-8" language="JAVA" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
        <fmt:requestEncoding value="utf-8"/>
        <fmt:setBundle basename="ResourceBundle"/>
        <title>
            <fmt:message key="result"/>
        </title>
        <link href="css/result_page.css" rel="stylesheet">
        <link href="css/common.css" rel="stylesheet">
    </head>
    <body>


        <div>
            <header>
                <div class="header-button-box">
                    <form action="<c:url value="/servlet"/>" method="get">
                        <button class="header-button" type="submit" value="back">
                            <fmt:message key="main_page"/>
                        </button>
                    </form>
                </div>
                <div class="header-button-box">
                    <form action="<c:url value="/servlet"/>" method="post">
                        <button class="header-button" type="submit" name="page" value="COMPETITION_LIST">
                            <fmt:message key="competitions"/>
                        </button>
                    </form>
                </div>

                <c:if test="${sessionScope.login eq null}">
                    <div class="header-button-box">
                        <form action="#LogInModal">
                            <button class="header-button" type="submit">
                                <fmt:message key="log_in"/>
                            </button>
                        </form>
                    </div>
                    <div class="header-button-box">
                        <form action="#SignUpModal">
                            <button class="header-button" type="submit">
                                <fmt:message key="sign_up"/>
                            </button>
                        </form>
                    </div>
                </c:if>

                <c:if test="${sessionScope.login eq null}">
                    <div id="LogInModal" class="modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">
                                        <fmt:message key="log_in"/>
                                    </h3>
                                    <a href="#close" title="Close" class="close">×</a>
                                </div>
                                <div class="modal-body">

                                    <div class="form">
                                        <form method="post" action="<c:url value="/servlet"/>">

                                            <input class="text-field" type="text" required
                                                   placeholder="<fmt:message key="login"/>"
                                                   name="login" pattern="[\w\d]{1,}"><br>
                                            <input class="text-field" type="password" required
                                                   placeholder="<fmt:message key="password"/>"
                                                   name="password" pattern="[\w\d]{1,}"><br><br>
                                            <button class="designed_button" type="submit" name="page" value="CABINET">
                                                <fmt:message key="log_in"/>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="SignUpModal" class="modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">
                                        <fmt:message key="sign_up"/>
                                    </h3>
                                    <a href="#close" title="Close" class="close">×</a>
                                </div>
                                <div class="modal-body">

                                    <div class="form">
                                        <form method="post" action="<c:url value="/servlet"/>">
                                            <input class="text-field" type="text" required
                                                   placeholder="<fmt:message key="login"/>"
                                                   name="newlogin" pattern="[\w\d]{1,}"><br>
                                            <input class="text-field" type="password" required
                                                   placeholder="<fmt:message key="password"/>"
                                                   name="newpassword" pattern="[\w\d]{1,}"><br><br>
                                                <%--                                    <input type="submit" hidden name="page" value="REGISTRATION">--%>
                                            <button class="designed_button" type="submit" name="page" value="REGISTRATION">
                                                <fmt:message key="sign_up"/>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </c:if>


                <c:if test="${sessionScope.login ne null}">
                    <div class="header-button-box">
                        <form action="<c:url value="/servlet"/>" method="post">
                                <%--                    <input type="submit" name="page" value="CABINET"/>--%>
                            <button class="header-button" type="submit" name="page" value="CABINET">
                                <fmt:message key="cabinet"/>
                            </button>
                        </form>
                    </div>
                </c:if>

                <c:if test="${sessionScope.login ne null}">
                    <div class="header-button-box">
                        <form action="<c:url value="/servlet"/>" method="post">
                            <button class="header-button" type="submit" name="page" onclick="update()" value="LOGOUT">
                                <fmt:message key="logout"/>
                            </button>
                        </form>
                    </div>
                </c:if>
                <script>
                    function update() {
                    window.location.reload();
                    }
                </script>
            </header>
        </div>


        <div class="main-content">
            <div class="event-info">
                <div class="event-name">
                        ${fn:escapeXml(requestScope.event.name)}
                </div>
                <div class="event-description">
                        ${fn:escapeXml(requestScope.event.description)}
                </div>
                <div class="event-date">
                        ${fn:escapeXml(requestScope.event.startDate)} - ${fn:escapeXml(requestScope.event.endDate)}
                        <br/>

                </div>
            </div>

            <c:if test="${requestScope.results[0] eq null}">
                <div class="no-data-message">
                    <fmt:message key="no_data_yet"/>
                </div>
            </c:if>

            <c:if test="${requestScope.results[0] ne null}">

                <table cellpadding="0" cellspacing="0">
                    <tr class="table-head">
                        <td class="head-position-label">
                            <fmt:message key="position"/>
                        </td>
                        <td class="head-car-number-label">
                            #
                        </td>
                        <td class="head-driver-label">
                            <fmt:message key="driver"/>
                        </td>
                        <td class="head-codriver-label">
                            <fmt:message key="codriver"/>
                        </td>
                        <td class="head-car-label">
                            <fmt:message key="car"/>
                        </td>
                        <td class="head-time-label">
                            <fmt:message key="time"/>
                        </td>
                    </tr>
                    <c:set var="count" value="1" scope="page"/>
                    <c:forEach var="result" items="${requestScope.results}">
                        <tr class="table-row">
                            <td class="position-label">
                                <c:if test="${result.time.finished eq true}">
                                    ${fn:escapeXml(count)}
                                </c:if>
                                <c:set var="count" value="${fn:escapeXml(count + 1)}" scope="page"/>
                            </td>
                            <td class="car-number-label">
                                    ${fn:escapeXml(result.team.carNumber)}
                            </td>
                            <td class="driver-label">
                                    ${fn:escapeXml(result.team.driver.name)}
                            </td>
                            <td class="codriver-label">
                                    ${fn:escapeXml(result.team.codriver.name)}
                            </td>
                            <td class="car-label">
                                    ${fn:escapeXml(result.team.car)}
                            </td>
                            <td class="time-label">
                                <c:if test="${result.time.finished eq true}">
                                    ${fn:escapeXml(result.timeString)}
                                </c:if>
                                <c:if test="${result.time.finished eq false}">
                                    <fmt:message key="lost_the_race"/>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </table>

            </c:if>

        </div>
    </body>
</html>
