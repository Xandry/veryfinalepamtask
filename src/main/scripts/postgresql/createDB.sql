-- MySQL Workbench Forward Engineering

/* SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0; */
/* SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0; */
/* SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES'; */

-- -----------------------------------------------------
-- Schema rally
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema rally
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS rally;
--USE rally;

-- -----------------------------------------------------
-- Table `rally`.`Event`
-- -----------------------------------------------------
CREATE SEQUENCE rally.Event_seq;

CREATE TABLE IF NOT EXISTS rally.Event
(
    idEvent     INT          NOT NULL DEFAULT NEXTVAL ('rally.Event_seq'),
    Name        VARCHAR(45)  NOT NULL,
    Description VARCHAR(250) NULL,
    Country     VARCHAR(45)  NULL,
    isOpen      BOOLEAN   NOT NULL,
    StartDate   DATE         NOT NULL,
    EndDate     DATE         NOT NULL,
    PRIMARY KEY (idEvent)
)
;


-- -----------------------------------------------------
-- Table `rally`.`Event_has_Pilot`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS rally.Event_has_Pilot
(
    Event_idEvent INT NOT NULL,
    Pilot_idPilot INT NOT NULL,
    PRIMARY KEY (Event_idEvent, Pilot_idPilot)
    ,
    CONSTRAINT fk_Event_has_Pilot_Event
        FOREIGN KEY (Event_idEvent)
            REFERENCES rally.Event (idEvent)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
;

CREATE INDEX fk_Event_has_Pilot_Event_idx ON rally.Event_has_Pilot (Event_idEvent ASC);


-- -----------------------------------------------------
-- Table `rally`.`Pilot`
-- -----------------------------------------------------
CREATE SEQUENCE rally.Pilot_seq;

CREATE TABLE IF NOT EXISTS rally.Pilot
(
    idPilot INT         NOT NULL DEFAULT NEXTVAL ('rally.Pilot_seq'),
    Name    VARCHAR(45) NOT NULL,
    PRIMARY KEY (idPilot)
)
;


-- -----------------------------------------------------
-- Table `rally`.`Team`
-- -----------------------------------------------------
CREATE SEQUENCE rally.Team_seq;

CREATE TABLE IF NOT EXISTS rally.Team
(
    idTeam     INT          NOT NULL DEFAULT NEXTVAL ('rally.Team_seq'),
    CarNumber  INT          NOT NULL,
    Car        VARCHAR(100) NOT NULL,
    idDriver   INT          NOT NULL,
    idCodriver INT          NOT NULL,
    PRIMARY KEY (idTeam)
    ,
    CONSTRAINT fk_Team_Pilot1
        FOREIGN KEY (idDriver)
            REFERENCES rally.Pilot (idPilot)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT fk_Team_Pilot2
        FOREIGN KEY (idCodriver)
            REFERENCES rally.Pilot (idPilot)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
;

CREATE INDEX fk_Team_Pilot1_idx ON rally.Team (idDriver ASC);
CREATE INDEX fk_Team_Pilot2_idx ON rally.Team (idCodriver ASC);


-- -----------------------------------------------------
-- Table `rally`.`Event_has_Team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS rally.Event_has_Team
(
    Event_idEvent         INT NOT NULL,
    Event_League_idLeague INT NOT NULL,
    Team_idTeam           INT NOT NULL,
    Team_idDriver         INT NOT NULL,
    Team_idCoDriver       INT NOT NULL,
    PRIMARY KEY (Event_idEvent, Event_League_idLeague, Team_idTeam, Team_idDriver, Team_idCoDriver)
    ,
    CONSTRAINT fk_Event_has_Team_Event1
        FOREIGN KEY (Event_idEvent)
            REFERENCES rally.Event (idEvent)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
;

CREATE INDEX fk_Event_has_Team_Event1_idx ON rally.Event_has_Team (Event_idEvent ASC, Event_League_idLeague ASC);


-- -----------------------------------------------------
-- Table `rally`.`Result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS rally.Result
(
    idEvent    INT        NOT NULL,
    Time       INT        NOT NULL,
    isFinished BOOLEAN NOT NULL,
    idTeam     INT        NOT NULL
    ,
    PRIMARY KEY (idEvent, idTeam)
    ,
    CONSTRAINT fk_Result_Event1
        FOREIGN KEY (idEvent)
            REFERENCES rally.Event (idEvent)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT fk_Result_Team1
        FOREIGN KEY (idTeam)
            REFERENCES rally.Team (idTeam)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
;

CREATE INDEX fk_Result_Event1_idx ON rally.Result (idEvent ASC);
CREATE INDEX fk_Result_Team1_idx ON rally.Result (idTeam ASC);


-- -----------------------------------------------------
-- Table `rally`.`Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS rally.Status
(
    idStatus    INT         NOT NULL,
    Description VARCHAR(45) NULL,
    PRIMARY KEY (idStatus)
)
;


-- -----------------------------------------------------
-- Table `rally`.`User`
-- -----------------------------------------------------
CREATE SEQUENCE rally.User_seq;

CREATE TABLE IF NOT EXISTS rally.User
(
    idUser       INT          NOT NULL DEFAULT NEXTVAL ('rally.User_seq'),
    Login        VARCHAR(45)  NOT NULL,
    PasswordHash VARCHAR(100) NOT NULL,
    idStatus     INT          NOT NULL,
    Salt         VARCHAR(45)  NOT NULL,
    idMyTeam     INT          NULL,
    PRIMARY KEY (idUser)
    ,
    CONSTRAINT fk_Users_Status1
        FOREIGN KEY (idStatus)
            REFERENCES rally.Status (idStatus)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT fk_User_Team1
        FOREIGN KEY (idMyTeam)
            REFERENCES rally.Team (idTeam)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
;

CREATE INDEX fk_Users_Status1_idx ON rally.User (idStatus ASC);
CREATE INDEX fk_User_Team1_idx ON rally.User (idMyTeam ASC);


/* SET SQL_MODE = @OLD_SQL_MODE; */
/* SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS; */
/* SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS; */
