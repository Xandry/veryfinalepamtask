use rally;

start transaction;

insert into result(idEvent, Time, isFinished, idTeam)
values (8, 127472, true, 5),
       (8, 127609, true, 158),
       (8, 127798, true, 9),
       (8, 127807, true, 136),
       (8, 128373, true, 3),
       (8, 128839, true, 1),
       (8, 129268, true, 138),
       (8, 130273, true, 7),
       (8, 132518, true, 107),
       (8, 132764, true, 153),
       (8, 134202, true, 152),
       (8, 134619, true, 10),
       (8, 134748, true, 150),
       (8, 134937, true, 149),
       (8, 136546, true, 146),
       (8, 137136, true, 145),
       (8, 137162, true, 144),
       (8, 137904, true, 2),
       (8, 139832, true, 6),
       (8, 141619, true, 124),
       (8, 145562, true, 69),
       (8, 145711, true, 62),
       (8, 147634, true, 88),
       (8, 148337, true, 36),
       (8, 148911, true, 67),
       (8, 149580, true, 133),
       (8, 150127, true, 132),
       (8, 150951, true, 61),
       (8, 156190, true, 130),
       (8, 156208, true, 65),
       (8, 157501, true, 128),
       (8, 157873, true, 127),
       (8, 158977, true, 126),
       (8, 160603, true, 17),
       (8, 160739, true, 115),
       (8, 162626, true, 59),
       (8, 166911, true, 113),
       (8, 167982, true, 112),
       (8, 168996, true, 111),
       (8, 173209, true, 110),
       (8, 177257, true, 8),
       (8, 178828, true, 106),
       (8, 182181, true, 108),
       (8, 182875, true, 104),
       (8, 184233, true, 103),
       (8, 184792, true, 102),
       (8, 194082, true, 101),
       (8, 201071, true, 100),
       (8, 209482, true, 99),
       (8, -1, false, 33),
       (8, -1, false, 97),
       (8, -1, false, 18),
       (8, -1, false, 87),
       (8, -1, false, 71),
       (8, -1, false, 70),
       (8, -1, false, 84),
       (8, -1, false, 83),
       (8, -1, false, 82),
       (8, -1, false, 81),
       (8, -1, false, 80),
       (8, -1, false, 79),
       (8, -1, false, 78),
       (8, -1, false, 77),
       (8, -1, false, 76),
       (8, -1, false, 75),
       (8, -1, false, 74),
       (8, -1, false, 58),
       (8, -1, false, 57),
       (8, -1, false, 56),
       (8, -1, false, 55),
       (8, -1, false, 54),
       (8, -1, false, 53),
       (8, -1, false, 52),
       (8, -1, false, 51),
       (8, -1, false, 50),
       (8, -1, false, 49),
       (8, -1, false, 48),
       (8, -1, false, 47);
commit;