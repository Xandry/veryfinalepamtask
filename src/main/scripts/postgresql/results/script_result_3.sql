use rally;

start transaction;

insert into result(idEvent, Time, isFinished, idTeam)
values (3, 130280, true, 8),
       (3, 130582, true, 3),
       (3, 130779, true, 136),
       (3, 131150, true, 1),
       (3, 133942, true, 7),
       (3, 139635, true, 200),
       (3, 141595, true, 149),
       (3, 141639, true, 6),
       (3, 143921, true, 5),
       (3, 146097, true, 339),
       (3, 157511, true, 40),
       (3, 161902, true, 199),
       (3, 168485, true, 138),
       (3, 190051, true, 335),
       (3, 197664, true, 334),
       (3, -1, false, 4),
       (3, -1, false, 332),
       (3, -1, false, 331),
       (3, -1, false, 330),
       (3, -1, false, 329),
       (3, -1, false, 328),
       (3, -1, false, 9),
       (3, -1, false, 12);
commit;