use rally;

start transaction;

insert into rally.status (idStatus, Description)
values (0, 'admin'),
       (1, 'user'),
       (2, 'unknown');

insert into rally.user(Login, PasswordHash, idStatus, Salt, idMyTeam)
VALUES ('admin', '|гяЫк–Ха(h‹Ыow;»u/[2…щ+&ЯClГ)¶·[ТЯPю®НжґЕ’do¤uVу*;0ъЖ]дъ', 0, 'sтђЎЩ”яPtЃ‹ЧTХЪџ', NULL), -- 123
       ('userb', 'ф}w•дЌА1Шп
КъКЎЙMzюіђкХЅyйд@¤ь&ђ¦ЧfЭЊк¶s%эїВYЁ]]®[Sт9Кы3‹™', 1, '‰0EvЌЛZ¶4¬щЊМ', NULL),               -- 123
       ('usera', '^рv(^r IДаvЕMЧe–
f† њ‹ol>6QkX*C>љІГ¦Ўн+Г№Э$MҐЃ™с[Їу�ш”дР0‡›Ъ', 1, 'V©ЩнCьЯUЋ№м-ЩHЂ', NULL); -- 123
commit;

