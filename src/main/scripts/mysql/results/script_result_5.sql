use rally;

start transaction;

insert into result(idEvent, Time, isFinished, idTeam)
values (5, 120546, true, 1),
       (5, 121030, true, 9),
       (5, 121194, true, 8),
       (5, 121208, true, 7),
       (5, 121357, true, 6),
       (5, 121413, true, 5),
       (5, 123519, true, 4),
       (5, 129194, true, 3),
       (5, 129231, true, 2),
       (5, 132691, true, 234),
       (5, 136921, true, 427),
       (5, 138439, true, 200),
       (5, 139401, true, 241),
       (5, 141151, true, 199),
       (5, 141854, true, 106),
       (5, 143607, true, 33),
       (5, 186808, true, 426),
       (5, 187804, true, 425),
       (5, 189784, true, 424),
       (5, 192670, true, 423),
       (5, 194715, true, 422),
       (5, -1, false, 138),
       (5, -1, false, 149),
       (5, -1, false, 136),
       (5, -1, false, 10);
commit;