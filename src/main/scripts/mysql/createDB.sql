-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema rally
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema rally
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rally` DEFAULT CHARACTER SET utf8;
USE `rally`;

-- -----------------------------------------------------
-- Table `rally`.`Event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`Event`
(
    `idEvent`     INT          NOT NULL AUTO_INCREMENT,
    `Name`        VARCHAR(45)  NOT NULL,
    `Description` VARCHAR(250) NULL,
    `Country`     VARCHAR(45)  NULL,
    `isOpen`      TINYINT(1)   NOT NULL,
    `StartDate`   DATE         NOT NULL,
    `EndDate`     DATE         NOT NULL,
    PRIMARY KEY (`idEvent`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rally`.`Event_has_Pilot`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`Event_has_Pilot`
(
    `Event_idEvent` INT NOT NULL,
    `Pilot_idPilot` INT NOT NULL,
    PRIMARY KEY (`Event_idEvent`, `Pilot_idPilot`),
    INDEX `fk_Event_has_Pilot_Event_idx` (`Event_idEvent` ASC),
    CONSTRAINT `fk_Event_has_Pilot_Event`
        FOREIGN KEY (`Event_idEvent`)
            REFERENCES `rally`.`Event` (`idEvent`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rally`.`Pilot`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`Pilot`
(
    `idPilot` INT         NOT NULL AUTO_INCREMENT,
    `Name`    VARCHAR(45) NOT NULL,
    PRIMARY KEY (`idPilot`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rally`.`Team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`Team`
(
    `idTeam`     INT          NOT NULL AUTO_INCREMENT,
    `CarNumber`  INT          NOT NULL,
    `Car`        VARCHAR(100) NOT NULL,
    `idDriver`   INT          NOT NULL,
    `idCodriver` INT          NOT NULL,
    PRIMARY KEY (`idTeam`),
    INDEX `fk_Team_Pilot1_idx` (`idDriver` ASC),
    INDEX `fk_Team_Pilot2_idx` (`idCodriver` ASC),
    CONSTRAINT `fk_Team_Pilot1`
        FOREIGN KEY (`idDriver`)
            REFERENCES `rally`.`Pilot` (`idPilot`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_Team_Pilot2`
        FOREIGN KEY (`idCodriver`)
            REFERENCES `rally`.`Pilot` (`idPilot`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rally`.`Event_has_Team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`Event_has_Team`
(
    `Event_idEvent`         INT NOT NULL,
    `Event_League_idLeague` INT NOT NULL,
    `Team_idTeam`           INT NOT NULL,
    `Team_idDriver`         INT NOT NULL,
    `Team_idCoDriver`       INT NOT NULL,
    PRIMARY KEY (`Event_idEvent`, `Event_League_idLeague`, `Team_idTeam`, `Team_idDriver`, `Team_idCoDriver`),
    INDEX `fk_Event_has_Team_Event1_idx` (`Event_idEvent` ASC, `Event_League_idLeague` ASC),
    CONSTRAINT `fk_Event_has_Team_Event1`
        FOREIGN KEY (`Event_idEvent`)
            REFERENCES `rally`.`Event` (`idEvent`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rally`.`Result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`Result`
(
    `idEvent`    INT        NOT NULL,
    `Time`       INT        NOT NULL,
    `isFinished` TINYINT(1) NOT NULL,
    `idTeam`     INT        NOT NULL,
    INDEX `fk_Result_Event1_idx` (`idEvent` ASC),
    PRIMARY KEY (`idEvent`, `idTeam`),
    INDEX `fk_Result_Team1_idx` (`idTeam` ASC),
    CONSTRAINT `fk_Result_Event1`
        FOREIGN KEY (`idEvent`)
            REFERENCES `rally`.`Event` (`idEvent`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_Result_Team1`
        FOREIGN KEY (`idTeam`)
            REFERENCES `rally`.`Team` (`idTeam`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rally`.`Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`Status`
(
    `idStatus`    INT         NOT NULL,
    `Description` VARCHAR(45) NULL,
    PRIMARY KEY (`idStatus`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rally`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rally`.`User`
(
    `idUser`       INT          NOT NULL AUTO_INCREMENT,
    `Login`        VARCHAR(45)  NOT NULL,
    `PasswordHash` VARCHAR(100) NOT NULL,
    `idStatus`     INT          NOT NULL,
    `Salt`         VARCHAR(45)  NOT NULL,
    `idMyTeam`     INT          NULL,
    PRIMARY KEY (`idUser`),
    INDEX `fk_Users_Status1_idx` (`idStatus` ASC),
    INDEX `fk_User_Team1_idx` (`idMyTeam` ASC),
    CONSTRAINT `fk_Users_Status1`
        FOREIGN KEY (`idStatus`)
            REFERENCES `rally`.`Status` (`idStatus`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_User_Team1`
        FOREIGN KEY (`idMyTeam`)
            REFERENCES `rally`.`Team` (`idTeam`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
