use rally;

start transaction;

insert into event(idEvent, Name, Description, Country, isOpen, StartDate, EndDate)
VALUES (1, 'Rallye Monte-Carlo', 'asphalt - snow 323.83 km', 'Monaco', FALSE, '2019-01-24', '2019-01-27'),
       (2, 'Rally Sweden', 'snow - ice 316.80 km', 'Sweden', FALSE, '2019-02-14', '2019-02-17'),
       (3, 'Rally Guanajuato México', 'gravel - 313.87 km', 'Mexico', FALSE, '2019-03-07', '2019-03-10'),
       (4, 'Corsica Linea - Tour de Corse', 'asphalt 347.17 km', 'France', FALSE, '2019-03-28', '2019-03-31'),
       (5, 'Xion Rally Argentina', 'rug - 347.50 km', 'Argentina', FALSE, '2019-04-25', '2019-04-28'),
       (6, 'COPEC Rally Chile', 'asphalt - 304.81 km', 'Chile', FALSE, '2019-05-09', '2019-05-12'),
       (7, 'Vodafone Rally de Portugal', 'gravel - 306.97 km', 'Portugal', false, '2019-05-30', '2019-06-02'),
       (8, 'Rally Italia Sardegna', 'gravel - 321.94 km', 'Italy', FALSE, '2019-06-13', '2019-06-16'),
       (9, 'Neste Rally Finland', 'gravel - 311.51 km', 'Finland', false, '2019-08-01', '2019-08-04'),
       (10, 'ADAC Rallye Deutschland', 'asphalt - 360.16 km', 'Germany', false, '2019-08-22', '2019-08-25'),
       (11, 'Rally Turkey', 'gravel - 346.18 km', 'Turkey', false, '2019-09-12', '2019-09-15'),
       (12, 'Wales Rally GB', 'gravel - 318.15 km', 'Great Britain', false, '2019-10-03', '2019-10-06'),
       (13, 'RallyRACC - Rally de España', 'gravel - snow 318.87 km', 'Spain', false, '2019-10-24', '2019-10-27'),
       (14, 'Kennards Hire Rally Australia', 'gravel - 327.18 km', 'Australia', false, '2019-11-14', '2019-11-17');

commit;


