package data.connectionpool;

import util.mysql.ErrorReadingPropertiesFileException;
import util.mysql.MysqlPropertiesFileHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Singleton class of connection pool.
 * Has {@code POOL_SIZE} already opened connections to the database.
 * if client want to get connection in case of lack opened connections, client locked by ArrayBlockingQueue.
 */
public class ConnectionPool {
    private static final Logger logger = LogManager.getLogger(ConnectionPool.class);
    private final static int POOL_SIZE = 20;
    private static ConnectionPool POOL = null;
    private ArrayBlockingQueue<Connection> connections;
    private Lock poolLocker;

    /**
     * Private constructor of singleton class.
     * Opens .prop file and gets database parameters (URL, login, password).
     *
     * @throws ErrorReadingPropertiesFileException if any error occupies while .prop file reading.
     * @see MysqlPropertiesFileHandler
     */
    private ConnectionPool() throws ErrorReadingPropertiesFileException {
        logger.debug("Creating connection pool. Pool size = " + POOL_SIZE);

        connections = new ArrayBlockingQueue<>(POOL_SIZE);
        poolLocker = new ReentrantLock();

        logger.debug("Reading .prop database's file...");
        MysqlPropertiesFileHandler handler = new MysqlPropertiesFileHandler();
        logger.trace("Database's property file has been successfully read.");

        final String URL = handler.getUrl();
        final String LOGIN = handler.getLogin();
        final String PASSWORD = handler.getPassword();

        for (int i = 0; i < POOL_SIZE; i++) {
            Connection connection;
            try {
                DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
                connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
                connections.offer(connection);
            } catch (SQLException e) {
                logger.fatal(e + " while creating connection pool.");
            }
        }
    }

    /**
     * Returns instance of connection pool. Initialises it if it's necessary.
     * @return Connection pool instance.
     * @throws ErrorReadingPropertiesFileException if any error
     *      * during reading config file occupies.
     */
    public static ConnectionPool getConnectionPool() throws ErrorReadingPropertiesFileException {
        if (POOL == null) {
            POOL = new ConnectionPool();
        }
        return POOL;
    }

    public static int getPoolSize() {
        return POOL_SIZE;
    }

    /**
     * Initialises connection pool.
     * @throws ErrorReadingPropertiesFileException if any error
     * during reading config file occupies.
     */
    public static void init() throws ErrorReadingPropertiesFileException {
        if (POOL == null) {
            new ConnectionPool();
        }
    }

    /**
     * @return connection to the database.
     * @throws ConnectionException if any error occupies.
     */
    public Connection getConnection() throws ConnectionException {
        Connection connection;
        try {
            connection = connections.take();
            if (connections.isEmpty()) {
                logger.warn("Connection pool is empty. All others clients are waiting for free connection.");
            }
        } catch (InterruptedException e) {
            logger.error(e + " while getting connection.");
            throw new ConnectionException(e);
        }
        return connection;
    }

    /**
     * Returns connection to pool so it can be taken by other client.
     *
     * @param connection connection to be returned.
     */
    public void returnConnectionToPool(Connection connection) {
        if (connection != null) {
            try {
                poolLocker.lock();
                if (!connections.contains(connection)) {
                    connections.offer(connection);
                }
            } finally {
                poolLocker.unlock();
            }
        }
    }

    /**
     * Closing all connections of the pool.
     *
     * @throws ConnectionException if any error occupies.
     */
    public void closeAll() throws ConnectionException {
        for (Connection connection : connections) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.fatal(e + " while closing all connections.");
                throw new ConnectionException(e);
            }
        }
    }
}
