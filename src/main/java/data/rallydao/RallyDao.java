package data.rallydao;

import application.rally.Event;
import application.rally.Pilot;
import application.rally.Result;
import application.rally.Team;
import data.rallydao.exception.AddDataDaoErrorException;
import data.rallydao.exception.DataCollectionDaoErrorException;
import data.rallydao.exception.DeleteDataDaoErrorException;

import java.util.ArrayList;

public interface RallyDao {
    ArrayList<Team> getTeams() throws DataCollectionDaoErrorException;

    Team getTeamById(int id) throws DataCollectionDaoErrorException;

    Team addTeamAndSetCorrectID(Team team) throws AddDataDaoErrorException;

    ArrayList<Event> getEventsWithTeamIdParticipated(int idTeam) throws DataCollectionDaoErrorException;

    ArrayList<Event> getEvents() throws DataCollectionDaoErrorException;

    Event getEventById(int id) throws DataCollectionDaoErrorException;

    Event addEventAndSetCorrectId(Event event) throws AddDataDaoErrorException;


    Pilot getPilot(int id) throws DataCollectionDaoErrorException;

    Pilot addPilotAndSetCorrectID(Pilot pilot) throws AddDataDaoErrorException;


    ArrayList<Result> getResults(int idEvent) throws DataCollectionDaoErrorException;

    void addResultByParameters(int idEvent, int time, int idTeam) throws AddDataDaoErrorException;

    void deleteResultByTeamIdEventId(int teamId, int eventID) throws DeleteDataDaoErrorException;

}
