package data.rallydao.mysql;

import application.rally.*;
import data.connectionpool.ConnectionException;
import data.connectionpool.ConnectionPool;
import data.rallydao.RallyDao;
import data.rallydao.exception.AddDataDaoErrorException;
import data.rallydao.exception.DataCollectionDaoErrorException;
import data.rallydao.exception.DeleteDataDaoErrorException;
import util.mysql.ErrorReadingPropertiesFileException;
import util.mysql.MysqlUtil;
import util.mysql.rallyDB.TableName;
import util.mysql.rallyDB.columnName.EventColumnName;
import util.mysql.rallyDB.columnName.PilotColumnName;
import util.mysql.rallyDB.columnName.ResultColumnName;
import util.mysql.rallyDB.columnName.TeamColumnName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class that implements {@code RallyDao} and gets rally-data from the database.
 * Uses connection pool of {@code ConnectionPool} class and common "makeQuery" request of {@code MysqlUtil} class.
 *
 * @see ConnectionPool
 * @see MysqlUtil
 */

public class DatabaseRallyDao implements RallyDao {
    private static final Logger logger = LogManager.getLogger(DatabaseRallyDao.class);

    public DatabaseRallyDao() {
    }

    @Override
    public ArrayList<Team> getTeams() throws DataCollectionDaoErrorException {
        logger.debug("Getting information about teams...");

        final String sqlQuery = "SELECT * FROM " + TableName.TEAM.getTableName();

        ResultSet resultSet = null;
        Connection connection = null;

        ArrayList<Team> arrayList = new ArrayList<>();

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sqlQuery, new ArrayList<>());

            logger.trace("Retrieving team's data from database...");
            while (resultSet.next()) {
                arrayList.add(getOneTeamFromResultSet(resultSet));
            }

            logger.trace("Team's data has been successfully collected.");

        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {

            logger.error(e + " while collecting team's data.");
            throw new DataCollectionDaoErrorException(e);

        } finally {

            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException | ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool and closing result set in getTeams().");
                throw new DataCollectionDaoErrorException(e);
            }
        }

        return arrayList;
    }

    @Override
    public ArrayList<Result> getResults(int idEvent) throws DataCollectionDaoErrorException {
        logger.debug("Getting information about event #" + idEvent);

        String sqlQuery = "SELECT * FROM "
                + TableName.RESULT.getTableName()
                + " where " + ResultColumnName.EVENT_ID.getColumnName() + " = ?";

        ResultSet resultSet = null;
        Connection connection = null;
        ArrayList<Result> arrayList = new ArrayList<>();

        try {

            ArrayList<String> params = new ArrayList<>();
            params.add(String.valueOf(idEvent));
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sqlQuery, params);

            logger.trace("Retrieving result #" + idEvent + " data from database...");
            while (resultSet.next()) {
                arrayList.add(getOneResultFromResultSet(resultSet));
            }

            logger.trace("Result's #" + idEvent + " data has been successfully collected.");
        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {
            e.printStackTrace();
            logger.error(e + " while collecting result's #" + idEvent + " data.");
            throw new DataCollectionDaoErrorException(e);

        } finally {

            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException | ErrorReadingPropertiesFileException e) {
                e.printStackTrace();
                logger.error(e + " while returning connection to the pool and closing result set.");
                throw new DataCollectionDaoErrorException(e);
            }

        }
        return arrayList;
    }

    @Override
    public ArrayList<Event> getEvents() throws DataCollectionDaoErrorException {
        logger.debug("Getting information about events...");

        String sqlQuery = "SELECT * FROM " + TableName.EVENT;

        ResultSet resultSet = null;
        Connection connection = null;
        ArrayList<Event> arrayList = new ArrayList<>();

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sqlQuery, new ArrayList<>());

            while (resultSet.next()) {
                arrayList.add(getOneEventFromResultSet(resultSet));
            }

            logger.trace("Events's data has been successfully collected.");
//            resultSet.close();

        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {

            e.printStackTrace();
            logger.error(e + " while collecting events's data.");
            throw new DataCollectionDaoErrorException(e);

        } finally {

            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException | ErrorReadingPropertiesFileException e) {
                e.printStackTrace();
                logger.error(e + " while returning connection to the pool and closing result set in getEvents().");
                throw new DataCollectionDaoErrorException(e);
            }
        }

        return arrayList;
    }

    @Override
    public Event getEventById(int id) throws DataCollectionDaoErrorException {
        final String sql = "select * from " +
                TableName.EVENT.getTableName() +
                " where " +
                EventColumnName.ID.getColumnName() +
                " = ?";
        Event result;

        Connection connection = null;
        ResultSet resultSet = null;

        ArrayList<String> params = new ArrayList<>();
        params.add(String.valueOf(id));
        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, params);

            resultSet.next();
            result = getOneEventFromResultSet(resultSet);
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException | DataCollectionDaoErrorException e) {
            e.printStackTrace();
            logger.error(e + " while getting an event.");
            throw new DataCollectionDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (ErrorReadingPropertiesFileException | SQLException e) {
                throw new DataCollectionDaoErrorException(e);
            }
        }
        return result;
    }

    @Override
    public Pilot addPilotAndSetCorrectID(Pilot pilot) throws AddDataDaoErrorException {
        final String sqlQuery = "insert into " +
                TableName.PILOT.getTableName() +
                "(" + PilotColumnName.NAME.getColumnName() + ") value " +
                "(?);";
        logger.trace("Adding a new pilot " + pilot.getName());
        PreparedStatement statement;
        Connection connection = null;

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            statement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            statement.setObject(1, pilot.getName());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                pilot.setIdPilot(rs.getInt(1));
            }
            logger.debug("New pilot" + pilot.getName() + "has been added.");
            return pilot;
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException e) {
            e.printStackTrace();
            logger.error(e + " while adding a new pilot.");
            throw new AddDataDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool and closing result set in addPilotAndSetCorrectID().");
                throw new AddDataDaoErrorException(e);
            }
        }
    }

    @Override
    public Team addTeamAndSetCorrectID(Team team) throws AddDataDaoErrorException {
        final String sqlQuery = "insert into "
                + TableName.TEAM.getTableName()
                + "("
                + TeamColumnName.CAR_NUMBER.getColumnName()
                + ","
                + TeamColumnName.CAR.getColumnName()
                + ","
                + TeamColumnName.DRIVER_ID.getColumnName()
                + ","
                + TeamColumnName.CODRIVER_ID.getColumnName()
                + ") value " +
                "(?, ?, ?, ?)";

        logger.trace("Adding a new team...");
        PreparedStatement statement;
        Connection connection = null;

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();

            statement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

            statement.setObject(1, team.getCarNumber());
            statement.setObject(2, team.getCar());
            statement.setObject(3, team.getDriver().getIdPilot());
            statement.setObject(4, team.getCodriver().getIdPilot());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
            }
            logger.debug("New team " + team.getId() + " has been added.");
            return team;
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException e) {
            e.printStackTrace();
            logger.error(e + " while adding a new team.");
            throw new AddDataDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool and closing result set in addTeamAndSetCorrectID().");
                throw new AddDataDaoErrorException(e);
            }
        }
    }

    @Override
    public ArrayList<Event> getEventsWithTeamIdParticipated(int idTeam) throws DataCollectionDaoErrorException {
        final String sql = "select * from "
                + TableName.EVENT.getTableName()
                + " join "
                + TableName.RESULT.getTableName()
                + " on event.idEvent = result.idEvent where idTeam = ?;";
        ResultSet resultSet = null;
        Connection connection = null;
        ArrayList<Event> arrayList = new ArrayList<>();
        ArrayList<String> params = new ArrayList<>();
        params.add(String.valueOf(idTeam));
        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, params);

            while (resultSet.next()) {
                arrayList.add(getOneEventFromResultSet(resultSet));
            }

            logger.trace("Events's data has been successfully collected.");
        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {
            e.printStackTrace();
            logger.error(e + " while collecting events's data.");
            throw new DataCollectionDaoErrorException(e);

        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException | ErrorReadingPropertiesFileException e) {
                e.printStackTrace();
                logger.error(e + " while returning connection to the pool and closing result set in getEventsWithTeamIdParticipateds().");
                throw new DataCollectionDaoErrorException(e);
            }
        }
        return arrayList;
    }

    /**
     * @param resultSet result set to be parsed.
     *                  Names of tables and columns are gotten from {@code TableName} and {@code TeamColumnName} classes.
     * @return parsed {@code Team} entity class.
     * @throws DataCollectionDaoErrorException if any collection request error occupies.
     * @see Team
     */
    private Team getOneTeamFromResultSet(ResultSet resultSet) throws DataCollectionDaoErrorException {
        try {
            int idTeam = resultSet.getInt(TeamColumnName.ID.getColumnName());
            int idDriver = resultSet.getInt(TeamColumnName.DRIVER_ID.getColumnName());
            int idCodriver = resultSet.getInt(TeamColumnName.CODRIVER_ID.getColumnName());
            String car = resultSet.getString(TeamColumnName.CAR.getColumnName());
            int carNumber = resultSet.getInt(TeamColumnName.CAR_NUMBER.getColumnName());

            Pilot driver = getPilot(idDriver);
            Pilot codriver = getPilot(idCodriver);

            return new Team(idTeam, driver, codriver, carNumber, car);
        } catch (SQLException e) {
            throw new DataCollectionDaoErrorException(e);
        }
    }

    /**
     * @param resultSet result set to be parsed.
     *                  Names of tables and columns are gotten from {@code TableName} and {@code EventColumnName} classes.
     * @return parsed {@code Event} class.
     * @throws DataCollectionDaoErrorException if any database exception occupies.
     * @see Event
     */
    private Event getOneEventFromResultSet(ResultSet resultSet) throws DataCollectionDaoErrorException {
        try {
            int idEvent = resultSet.getInt(EventColumnName.ID.getColumnName());
            String name = resultSet.getString(EventColumnName.NAME.getColumnName()).trim();
            String description = resultSet.getString(EventColumnName.DESCRIPTION.getColumnName()).trim();
            String country = resultSet.getString(EventColumnName.COUNTRY.getColumnName()).trim();
            boolean isOpen = resultSet.getBoolean(EventColumnName.IS_OPEN.getColumnName());

            Date startDate = resultSet.getDate(EventColumnName.START_DATE.getColumnName());
            Date endDate = resultSet.getDate(EventColumnName.END_DATE.getColumnName());

            return new Event(idEvent, name, description, country, isOpen, startDate, endDate);
        } catch (SQLException e) {
            throw new DataCollectionDaoErrorException(e);
        }
    }

    /**
     * @param resultSet result set to be parsed.
     *                  Names of tables and columns are gotten from {@code TableName} and {@code ResultColumnName} classes.
     * @return parsed {@code Result} class.
     * @throws DataCollectionDaoErrorException if any database exception occupies.
     * @see Result
     */
    private Result getOneResultFromResultSet(ResultSet resultSet) throws DataCollectionDaoErrorException {
        try {
            int time = resultSet.getInt(ResultColumnName.TIME.getColumnName());
            int idEvent = resultSet.getInt(ResultColumnName.EVENT_ID.getColumnName());
            int idTeam = resultSet.getInt(ResultColumnName.TEAM_ID.getColumnName());
            return new Result(idEvent, new RaceTime(time), getTeamById(idTeam));
        } catch (SQLException e) {
            throw new DataCollectionDaoErrorException(e);
        }
    }

    /**
     * Get {@code Pilot} by EVENT_ID from the database - using {@code ConnectionPool}.
     * Names of tables and columns are gotten from {@code TableName} and {@code PilotColumnName} classes.
     *
     * @param id id of the pilot.
     * @return parsed {@code Pilot} class.
     * @throws DataCollectionDaoErrorException if any error during database data gaining occupies.
     * @see Pilot
     * @see ConnectionPool
     */
    @Override
    public Pilot getPilot(int id) throws DataCollectionDaoErrorException {
        final String sql = "select " +
                PilotColumnName.NAME.getColumnName() +
                " from " +
                TableName.PILOT.getTableName() +
                " where " +
                PilotColumnName.ID.getColumnName() +
                " = ?";
        String driverName;
        Connection connection = null;
        ResultSet resultSet = null;
        ArrayList<String> params = new ArrayList<>();
        params.add(String.valueOf(id));

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, params);
            resultSet.next();
            driverName = resultSet.getString(PilotColumnName.NAME.getColumnName());
        } catch (ErrorReadingPropertiesFileException | ConnectionException | SQLException e) {

            e.printStackTrace();
            logger.error(e + " while getting pilot.");
            throw new DataCollectionDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (ErrorReadingPropertiesFileException | SQLException e) {
                throw new DataCollectionDaoErrorException(e);
            }
        }
        return new Pilot(driverName, id);
    }

    @Override
    public Team getTeamById(int id) throws DataCollectionDaoErrorException {
        final String sql = "select * from " +
                TableName.TEAM.getTableName() +
                " where " +
                TeamColumnName.ID.getColumnName() +
                " = ?";
        Team result;

        Connection connection = null;
        ResultSet resultSet = null;

        ArrayList<String> params = new ArrayList<>();
        params.add(String.valueOf(id));
        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, params);

            resultSet.next();
            result = getOneTeamFromResultSet(resultSet);
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException | DataCollectionDaoErrorException e) {
            e.printStackTrace();
            logger.error(e + " while getting team.");
            throw new DataCollectionDaoErrorException(e);
        } finally {

            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (ErrorReadingPropertiesFileException | SQLException e) {
                throw new DataCollectionDaoErrorException(e);
            }

        }
        return result;
    }

    @Override
    public Event addEventAndSetCorrectId(Event event) throws AddDataDaoErrorException {
        final String sqlQuery = "insert into "
                + TableName.EVENT.getTableName()
                + "("
                + EventColumnName.NAME.getColumnName()
                + ", "
                + EventColumnName.DESCRIPTION.getColumnName()
                + ","
                + EventColumnName.COUNTRY.getColumnName()
                + ","
                + EventColumnName.IS_OPEN.getColumnName()
                + ","
                + EventColumnName.START_DATE.getColumnName()
                + ","
                + EventColumnName.END_DATE.getColumnName()
                + ") VALUE ( ?, ?, ?, ?, ?, ?)";

        logger.trace("Adding a new event...");
        PreparedStatement statement;
        Connection connection = null;

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            statement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

            statement.setObject(1, event.getName());
            statement.setObject(2, event.getDescription());
            statement.setObject(3, event.getCountry());
            statement.setBoolean(4, event.isOpen());
            statement.setDate(5, new java.sql.Date(event.getStartDate().getTime()));
            statement.setDate(6, new java.sql.Date(event.getEndDate().getTime()));
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                event.setIdEvent(rs.getInt(1));
            }
            logger.debug("New event #" + event.getIdEvent() + " has been added.");
            return event;
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException e) {
            e.printStackTrace();
            logger.error(e + " while adding a new event.");
            throw new AddDataDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool and closing result set in addEventAndSetCorrectId().");
                throw new AddDataDaoErrorException(e);
            }
        }
    }

    @Override
    public void addResultByParameters(int idEvent, int time, int idTeam) throws AddDataDaoErrorException {
        final String sqlQuery = "replace into "
                + TableName.RESULT.getTableName()
                + " ("
                + ResultColumnName.EVENT_ID.getColumnName()
                + ","
                + ResultColumnName.TIME.getColumnName()
                + ","
                + ResultColumnName.IS_FINISHED.getColumnName()
                + ","
                + ResultColumnName.TEAM_ID.getColumnName()
                + ") VALUE (?, ?, ?, ?)";

        logger.trace("Adding a new result...");
        PreparedStatement statement;
        Connection connection = null;

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            statement = connection.prepareStatement(sqlQuery);

            statement.setObject(1, idEvent);
            statement.setObject(2, time);
            statement.setBoolean(3, time != 0);
            statement.setObject(4, idTeam);
            statement.executeUpdate();
            logger.debug("New result has been added.");
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException e) {
            e.printStackTrace();
            logger.error(e + " while adding a new event.");
            throw new AddDataDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool set in addResultByParameters().");
                throw new AddDataDaoErrorException(e);
            }
        }
    }

    @Override
    public void deleteResultByTeamIdEventId(int teamId, int eventId) throws DeleteDataDaoErrorException {
        final String sqlQuery = "delete ignore from "
                + TableName.RESULT.getTableName()
                + " where "
                + ResultColumnName.TEAM_ID.getColumnName()
                + " = ? and "
                + ResultColumnName.EVENT_ID.getColumnName()
                + "= ?";

        logger.trace("Deleting result of team #" + teamId + " of event #" + eventId);
        PreparedStatement statement;
        Connection connection = null;

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            statement = connection.prepareStatement(sqlQuery);

            statement.setObject(1, teamId);
            statement.setObject(2, eventId);
            statement.executeUpdate();
            logger.debug("Result has been deleted.");
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException e) {
            e.printStackTrace();
            logger.error(e + " while deleting a result.");
            throw new DeleteDataDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool set in deleteResultByTeamIdEventId().");
                throw new DeleteDataDaoErrorException(e);
            }
        }
    }
}
