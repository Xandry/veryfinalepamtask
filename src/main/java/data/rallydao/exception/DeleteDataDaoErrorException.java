package data.rallydao.exception;

public class DeleteDataDaoErrorException extends Exception {
    public DeleteDataDaoErrorException() {
    }

    public DeleteDataDaoErrorException(String message) {
        super(message);
    }

    public DeleteDataDaoErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeleteDataDaoErrorException(Throwable cause) {
        super(cause);
    }
}
