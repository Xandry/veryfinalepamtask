package data.rallydao.exception;

public class AddDataDaoErrorException extends Exception {
    public AddDataDaoErrorException() {
    }

    public AddDataDaoErrorException(String message) {
        super(message);
    }

    public AddDataDaoErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddDataDaoErrorException(Throwable cause) {
        super(cause);
    }
}
