package data.rallydao.exception;

public class DataCollectionDaoErrorException extends Exception {

    public DataCollectionDaoErrorException() {
    }

    public DataCollectionDaoErrorException(String message) {
        super(message);
    }

    public DataCollectionDaoErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataCollectionDaoErrorException(Throwable cause) {
        super(cause);
    }
}
