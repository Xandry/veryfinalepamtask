package data.userdao.mysql;

import application.user.User;
import application.user.UserRole;
import data.connectionpool.ConnectionException;
import data.connectionpool.ConnectionPool;
import data.userdao.UserDao;
import data.userdao.exception.AddUserDaoErrorException;
import data.userdao.exception.DeleteUserDaoErrorException;
import data.userdao.exception.GetUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import security.Hasher;
import security.HashingAlgorithmException;
import util.mysql.ErrorReadingPropertiesFileException;
import util.mysql.MysqlUtil;
import util.mysql.rallyDB.TableName;
import util.mysql.rallyDB.columnName.UserColumnName;

import java.sql.*;
import java.util.ArrayList;

public class DatabaseUserDao implements UserDao {
    private static final Logger logger = LogManager.getLogger(DatabaseUserDao.class);

    private static final String USER_TABLE = TableName.USER.getTableName();

    @Override
    public User getById(int id) throws GetUserDaoErrorException, NoSuchUserException {
        final String sql = "select * from " + USER_TABLE +
                " where " + UserColumnName.ID.getColumnName() + " = ?;";

        logger.debug("Getting user #" + id);

        Connection connection = null;
        ResultSet resultSet = null;
        ArrayList<String> params = new ArrayList<>();
        params.add(String.valueOf(id));

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, params);
            return getUserFromResultSet(resultSet);
        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {
            logger.error(e + " during getting user #" + id);
            throw new GetUserDaoErrorException(e);

        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException | ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool and closing result set in getById().");
                throw new GetUserDaoErrorException(e);
            }
        }
    }

    @Override
    public String getSalt(String login) throws NoSuchUserException, GetUserDaoErrorException {
        final String sql = "select " +
                UserColumnName.PASSWORD_SALT.getColumnName() +
                " from " +
                TableName.USER.getTableName() +
                " where " +
                UserColumnName.LOGIN.getColumnName() +
                " = ?";
        logger.trace("Getting salt of user " + login);

        Connection connection = null;
        ResultSet resultSet = null;
        ArrayList<String> params = new ArrayList<>();
        params.add(login);

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, params);

            if (resultSet.next()) {
                String salt = resultSet.getString(UserColumnName.PASSWORD_SALT.getColumnName());
                logger.trace("User has been found.");
                return salt;
            } else {
                resultSet.close();
                logger.trace("User hasn't been found.");
                throw new NoSuchUserException();
            }

        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {
            logger.error(e + " during getting salt of a user by login.");
            throw new GetUserDaoErrorException(e.getMessage());
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (ErrorReadingPropertiesFileException | SQLException e) {
                e.printStackTrace();
                logger.error(e + " while returning connection to the pool and closing result set in getSalt().");
                throw new GetUserDaoErrorException(e);
            }

        }
    }

    /**
     * 'Safe way' means that no confidential data (password hash) going to be
     * sent to a client.
     *
     * @return Users without password hash data.
     * @throws GetUserDaoErrorException if any error occupies.
     */
    @Override
    public ArrayList<User> getSafeUsers() throws GetUserDaoErrorException {
        final String sql = "select "
                + UserColumnName.ID.getColumnName()
                + ","
                + UserColumnName.LOGIN.getColumnName()
                + ","
                + UserColumnName.STATUS_ID.getColumnName()
                + ","
                + UserColumnName.MY_TEAM_ID.getColumnName()
                + " from "
                + USER_TABLE;
        logger.trace("Getting all users in a safe way...");
        Connection connection = null;
        ResultSet resultSet = null;

        ArrayList<User> usersArrayList = new ArrayList<>();

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, new ArrayList<>());

            while (resultSet.next()) {
                usersArrayList.add(getUserFromResultSetWithoutHash(resultSet));
            }

        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException e) {
            logger.error(e + " while getting all users.");
            e.printStackTrace();
            throw new GetUserDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (ErrorReadingPropertiesFileException | SQLException e) {
                e.printStackTrace();
                logger.error(e + " while returning connection to the pool and closing result set in getSafeUsers().");
                throw new GetUserDaoErrorException(e);
            }
        }

        return usersArrayList;
    }

    @Override
    public void alterUser(int id, User user) throws AddUserDaoErrorException {
        final String sql = "update " +
                TableName.USER.getTableName() +
                " set " +
                UserColumnName.ID.getColumnName() +
                " = ?," +
                UserColumnName.MY_TEAM_ID.getColumnName() +
                "= ?, " +
                UserColumnName.STATUS_ID.getColumnName() +
                "= ?, " +
                UserColumnName.LOGIN.getColumnName() +
                "= ?, " +
                UserColumnName.PASSWORD_SALT.getColumnName() +
                "= ?, " +
                UserColumnName.PASSWORD_HASH.getColumnName() +
                "= ? where " +
                UserColumnName.ID.getColumnName() + " = ?";
        logger.trace("Editing a user...");
        PreparedStatement statement;
        Connection connection = null;

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            statement = connection.prepareStatement(sql);
            statement.setObject(1, user.getId());
            statement.setObject(2, user.getIdMyTeam());
            statement.setObject(3, UserRole.toInt(user.getRole()));
            statement.setObject(4, user.getLogin());
            statement.setObject(5, user.getSalt());
            statement.setObject(6, user.getPasswordHash());
            statement.setObject(7, id);

            statement.executeUpdate();
            logger.debug("User #" + id + " has been changed.");
        } catch (ConnectionException | SQLException | ErrorReadingPropertiesFileException e) {
            logger.error(e + " while editing a user.");
            e.printStackTrace();
            throw new AddUserDaoErrorException(e);
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool and closing result set in alterUser().");
                throw new AddUserDaoErrorException(e);
            }
        }
    }


    @Override
    public User getByLoginAndPassword(String login, String password)
            throws NoSuchUserException, GetUserDaoErrorException {

        String hash;
        String salt;
        try {
            salt = getSalt(login);
            Hasher hasher = new Hasher();
            hash = hasher.getHash(password, salt);
        } catch (HashingAlgorithmException e) {
            logger.error(e + " during getting user by login and password.");
            throw new GetUserDaoErrorException(e);
        }

        final String sql = "select * from " +
                USER_TABLE +
                " where " +
                UserColumnName.LOGIN.getColumnName() +
                " = ? and "
                + UserColumnName.PASSWORD_HASH.getColumnName() +
                "  = ?";

        logger.trace("Getting user by login and password");

        Connection connection = null;
        ResultSet resultSet = null;
        ArrayList<String> params = new ArrayList<>();
        params.add(login);
        params.add(hash);

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            resultSet = MysqlUtil.makeQuery(connection, sql, params);
            return getUserFromResultSet(resultSet);
        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {
            logger.error(e + " during getting user by login and password.");
            throw new GetUserDaoErrorException(e.getMessage());
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (ErrorReadingPropertiesFileException | SQLException e) {
                e.printStackTrace();
                logger.error(e + " while returning connection to the pool and closing result set in getByLoginAnd Password().");
                throw new GetUserDaoErrorException(e);
            }
        }
    }

    private User getUserFromResultSet(ResultSet resultSet) throws SQLException, NoSuchUserException {
        if (resultSet.next()) {
            logger.trace("User has been found.");
            int idUser = resultSet.getInt(UserColumnName.ID.getColumnName());
            String login = resultSet.getString(UserColumnName.LOGIN.getColumnName());
            String password = resultSet.getString(UserColumnName.PASSWORD_HASH.getColumnName());
            UserRole role = UserRole.parseInt(resultSet.getInt(UserColumnName.STATUS_ID.getColumnName()));
            String salt = resultSet.getString(UserColumnName.PASSWORD_SALT.getColumnName());
            Integer idMyTeam = resultSet.getInt(UserColumnName.MY_TEAM_ID.getColumnName());
            return new User(idUser, login, password, role, salt, idMyTeam);
//            return getUserFromResultSet(resultSet);
        } else {
            logger.trace("User hasn't been found.");
            throw new NoSuchUserException();
        }
    }

    /**
     * Returns user with empty {@code hash} and {@code salt} fields.
     *
     * @param resultSet source of the data. It won't be closed.
     * @return user without hash and salt data.
     * @throws SQLException if any error occupies.
     */
    private User getUserFromResultSetWithoutHash(ResultSet resultSet) throws SQLException {
        int idUser = resultSet.getInt(UserColumnName.ID.getColumnName());
        String login = resultSet.getString(UserColumnName.LOGIN.getColumnName());
        UserRole role = UserRole.parseInt(resultSet.getInt(UserColumnName.STATUS_ID.getColumnName()));
        Integer idMyTeam = resultSet.getInt(UserColumnName.MY_TEAM_ID.getColumnName());


        return new User(idUser, login, "", role, "", idMyTeam);
    }

    @Override
    public void addUser(User user) throws AddUserDaoErrorException {
        String sqlQuery = "insert into "
                + USER_TABLE +
                "("
                + UserColumnName.LOGIN.getColumnName()
                + ", "
                + UserColumnName.PASSWORD_HASH.getColumnName()
                + ", "
                + UserColumnName.STATUS_ID.getColumnName()
                + ", "
                + UserColumnName.PASSWORD_SALT.getColumnName()
                + ", "
                + UserColumnName.MY_TEAM_ID.getColumnName()
                + ") VALUE (?, ?, ?, ?, ?);";

        logger.trace("Adding a new user " + user.getLogin() + "...");

        PreparedStatement statement;
        Connection connection = null;

        try {
            connection = ConnectionPool.getConnectionPool().getConnection();

            statement = connection.prepareStatement(sqlQuery);
            statement.setObject(1, user.getLogin());
            statement.setObject(2, user.getPasswordHash());
            statement.setObject(3, UserRole.toInt(user.getRole()));
            statement.setObject(4, user.getSalt());
            if (user.getIdMyTeam() != null) {
                statement.setObject(5, user.getIdMyTeam());
            } else {
                statement.setNull(5, Types.INTEGER);
            }
//            statement.executeQuery();
            statement.executeUpdate();

            logger.trace("New user has been added.");
        } catch (SQLException | ErrorReadingPropertiesFileException | ConnectionException e) {

            logger.error(e + " during adding a new user.");
            throw new AddUserDaoErrorException(e);

        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool and closing result set in addNewUser().");
                throw new AddUserDaoErrorException(e);
            }
        }
    }

    @Override
    public void deleteUserById(int id) throws DeleteUserDaoErrorException {
        logger.trace("Deleting user #" + id);
        String sqlQuery = "delete from " + USER_TABLE + " where " + UserColumnName.ID.getColumnName() + " = " + id;
        Connection connection = null;
        PreparedStatement statement;
        try {
            connection = ConnectionPool.getConnectionPool().getConnection();
            statement = connection.prepareStatement(sqlQuery);
            statement.executeUpdate();
            logger.debug("User has been deleted");
        } catch (ConnectionException | ErrorReadingPropertiesFileException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ConnectionPool.getConnectionPool().returnConnectionToPool(connection);
            } catch (ErrorReadingPropertiesFileException e) {
                logger.error(e + " while returning connection to the pool in deleteUserById().");
                throw new DeleteUserDaoErrorException(e);
            }
        }
    }

}
