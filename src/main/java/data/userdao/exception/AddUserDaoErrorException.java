package data.userdao.exception;

public class AddUserDaoErrorException extends Exception {
    public AddUserDaoErrorException() {
    }

    public AddUserDaoErrorException(String message) {
        super(message);
    }

    public AddUserDaoErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddUserDaoErrorException(Throwable cause) {
        super(cause);
    }
}
