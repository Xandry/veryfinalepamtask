package data.userdao.exception;

public class DeleteUserDaoErrorException extends Exception {
    public DeleteUserDaoErrorException() {
    }

    public DeleteUserDaoErrorException(String message) {
        super(message);
    }

    public DeleteUserDaoErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeleteUserDaoErrorException(Throwable cause) {
        super(cause);
    }
}
