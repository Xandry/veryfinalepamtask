package data.userdao.exception;

public class GetUserDaoErrorException extends Exception {
    public GetUserDaoErrorException() {
    }

    public GetUserDaoErrorException(String message) {
        super(message);
    }

    public GetUserDaoErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public GetUserDaoErrorException(Throwable cause) {
        super(cause);
    }
}
