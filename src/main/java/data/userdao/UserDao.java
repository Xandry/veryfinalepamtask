package data.userdao;

import application.user.User;
import data.userdao.exception.AddUserDaoErrorException;
import data.userdao.exception.DeleteUserDaoErrorException;
import data.userdao.exception.GetUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;

import java.util.ArrayList;

public interface UserDao {

    User getById(int id) throws NoSuchUserException, GetUserDaoErrorException;

    User getByLoginAndPassword(final String login, final String password) throws NoSuchUserException, GetUserDaoErrorException;

    void addUser(final User user) throws AddUserDaoErrorException;

    void deleteUserById(int id) throws DeleteUserDaoErrorException;

    void alterUser(int id, User user) throws AddUserDaoErrorException;

    String getSalt(final String login) throws NoSuchUserException, GetUserDaoErrorException;

    ArrayList<User> getSafeUsers() throws GetUserDaoErrorException;

}
