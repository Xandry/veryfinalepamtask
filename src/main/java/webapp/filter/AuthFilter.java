package webapp.filter;

import application.user.User;
import application.user.UserRole;
import core.factory.PageType;
import data.userdao.UserDao;
import data.userdao.exception.GetUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;
import data.userdao.mysql.DatabaseUserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import webapp.AttributeName;
import webapp.state.AdminState;
import webapp.state.State;
import webapp.state.UnknownState;
import webapp.state.UserState;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Objects.nonNull;

@WebFilter("/servlet")
public class AuthFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(AuthFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain)
            throws IOException, ServletException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        final HttpSession session = req.getSession();

        String destinationPage = req.getParameter(AttributeName.PAGE);
        logger.trace("Client is looking for " + destinationPage);

        if (destinationPage == null) {
            request.setAttribute(AttributeName.PAGE, PageType.INDEX);
            filterChain.doFilter(request, response);
            return;
        }
        State state = getClientState(req, res, new DatabaseUserDao(), session);
        req.setAttribute(AttributeName.STATE, state);

        filterChain.doFilter(request, response);
    }

    /**
     * Gets information about user from Dao and create according State.
     *
     * @param request  request to filter.
     * @param response response of filter.
     * @param userDao  Dao that contains user's information.
     * @param session  current session.
     * @return One of the State implemented classes.
     */
    public State getClientState(HttpServletRequest request, HttpServletResponse response, UserDao userDao, HttpSession session) {
        final String login = request.getParameter(AttributeName.LOGIN);
        final String password = request.getParameter(AttributeName.PASSWORD);

        State resultState = new UnknownState();

        if (nonNull(session) &&
                nonNull(session.getAttribute(AttributeName.LOGIN)) &&
                nonNull(session.getAttribute(AttributeName.PASSWORD))) {
            //Logged user.

            final UserRole userRole = (UserRole) session.getAttribute(AttributeName.ROLE);
            if (userRole == UserRole.USER) {
                resultState = new UserState();
            }
            if (userRole == UserRole.ADMIN) {
                resultState = new AdminState();
            }
            return resultState;
        } else {
            try {
                // the account exists in the database - log in
                final User user = userDao.getByLoginAndPassword(login, password);
                final UserRole role = user.getRole();
                logger.trace("User " + login + " has been found in the database.");

                setIdLoginPasswordRole(session, String.valueOf(user.getId()), login, password, role);
                if (role == UserRole.USER) {
                    resultState = new UserState();
                }
                if (role == UserRole.ADMIN) {
                    resultState = new AdminState();
                }

            } catch (NoSuchUserException e) {
                // unauthorized user
                resultState = new UnknownState();

            } catch (GetUserDaoErrorException e) {
                logger.error(e + " while authorisation and authentication.");
                // authentication - if user valid
                // authorisation - access handling
                e.printStackTrace();
            }
        }
        return resultState;
    }

    /**
     * @param session  Session that about to be edited.
     * @param id       User's id.
     * @param login    User's login.
     * @param password User's password.
     * @param role     User's role.
     */
    private void setIdLoginPasswordRole(HttpSession session, String id, String login, String password, UserRole role) {
        if (session != null) {
            session.setAttribute(AttributeName.ID, id);
            session.setAttribute(AttributeName.PASSWORD, password);
            session.setAttribute(AttributeName.LOGIN, login);
            session.setAttribute(AttributeName.ROLE, role);
        }
    }

    @Override
    public void destroy() {
    }

}