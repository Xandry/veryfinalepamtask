package webapp;

public class Language {
    public static final String RUSSIAN = "ru_RU";
    public static final String ENGLISH = "en_US";

    public static String parseString(String s) {
        if (s.equals("RUSSIAN")) {
            return RUSSIAN;
        } else {
            return ENGLISH;
        }
    }
}
