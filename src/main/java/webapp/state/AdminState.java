package webapp.state;

import application.rally.Event;
import application.rally.RaceTime;
import application.rally.Team;
import application.rally.json.JsonFiledName;
import application.rally.validator.EventValidator;
import application.rally.validator.RaceTimeValidator;
import application.user.User;
import application.user.UserRole;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.factory.PageType;
import data.rallydao.RallyDao;
import data.rallydao.exception.AddDataDaoErrorException;
import data.rallydao.exception.DataCollectionDaoErrorException;
import data.rallydao.mysql.DatabaseRallyDao;
import data.userdao.UserDao;
import data.userdao.exception.DeleteUserDaoErrorException;
import data.userdao.exception.GetUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;
import data.userdao.mysql.DatabaseUserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import webapp.AttributeName;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AdminState implements State {

    private static final Logger logger = LogManager.getLogger(AdminState.class);

    @Override
    public void doRegistration(HttpServletRequest request, HttpServletResponse response)
            throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoCabinet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher(PageType.getPage(PageType.ADMIN_MENU)).forward(request, response);
    }

    @Override
    public void doLogout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        removeUserAttributes(request, response);
        request.getRequestDispatcher(PageType.getPage(PageType.INDEX)).forward(request, response);
    }


    @Override
    public void doDeleteUser(HttpServletRequest request, HttpServletResponse response)
            throws DeleteUserDaoErrorException, ServletException, NoSuchUserException, GetUserDaoErrorException, IOException {
        try {
            UserDao usersHandler = new DatabaseUserDao();
            int id = Integer.parseInt(request.getParameter(AttributeName.ID));
            if (id < 0) {
                throw new IllegalArgumentException("User ID can't be negative");
            }
            User user = usersHandler.getById(id);
            if (user.getRole() != UserRole.ADMIN) {
                usersHandler.deleteUserById(id);
            }
            request.getRequestDispatcher(PageType.getPage(PageType.ALL_USERS)).forward(request, response);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }


    @Override
    public void gotoUsersList(HttpServletRequest request, HttpServletResponse response)
            throws GetUserDaoErrorException, ServletException, IOException {
        try {
            UserDao usersHandler = new DatabaseUserDao();
            ArrayList<User> users = usersHandler.getSafeUsers();
            request.setAttribute(AttributeName.USERS, users);
            request.getRequestDispatcher(PageType.getPage(PageType.ALL_USERS)).forward(request, response);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    @Override
    public void doCreateTeam(HttpServletRequest request, HttpServletResponse response)
            throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoTeamCreation(HttpServletRequest request, HttpServletResponse response)
            throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoEventCreation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(PageType.getPage(PageType.GOTO_EVENT_CREATION)).forward(request, response);
    }

    private Event getEventFromRequest(HttpServletRequest request) throws ParseException {
        final String name = request.getParameter(AttributeName.NEW_EVENT_NAME);
        final String description = request.getParameter(AttributeName.NEW_EVENT_DESCRIPTION);
        final String country = request.getParameter(AttributeName.NEW_EVENT_COUNTRY);

        final boolean isOpen = Boolean.parseBoolean(request.getParameter(AttributeName.NEW_EVENT_IS_OPEN));
        final String startDateString = request.getParameter(AttributeName.NEW_EVENT_START_DATE);
        final String endDateString = request.getParameter(AttributeName.NEW_EVENT_END_DATE);

        Date start = new SimpleDateFormat("yyyy-MM-dd").parse(startDateString);
        Date end = new SimpleDateFormat("yyyy-MM-dd").parse(endDateString);
        return new Event(0, name, description, country, isOpen, start, end);
    }

    @Override
    public void doAddEvent(HttpServletRequest request, HttpServletResponse response)
            throws IllegalArgumentException, AddDataDaoErrorException {
        try {
            Event event = getEventFromRequest(request);
            new EventValidator().validate(event);

            RallyDao rallyDAO = new DatabaseRallyDao();
            rallyDAO.addEventAndSetCorrectId(event);

        } catch (ParseException e) {
            e.printStackTrace();
            logger.error("Add event error: " + e);
            throw new AddDataDaoErrorException(e);
        }
    }

    private String parseTeamsToJson(ArrayList<Team> teams) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(teams).replace("\'", "");
    }

    private String parseEventsToJson(ArrayList<Event> events) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(events).replace("\'", "");
    }

    @Override
    public void gotoResultEditing(HttpServletRequest request, HttpServletResponse response)
            throws DataCollectionDaoErrorException, IOException, ServletException {
        try {
            RallyDao rallyDAO = new DatabaseRallyDao();
            request.setAttribute(AttributeName.TEAMS, parseTeamsToJson(rallyDAO.getTeams()));
            request.setAttribute(AttributeName.EVENTS, parseEventsToJson(rallyDAO.getEvents()));
            request.getRequestDispatcher(PageType.getPage(PageType.GOTO_EDIT_RESULT)).forward(request, response);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    /**
     * The method gets RaceTime object out of string.
     *
     * @param timeString String to be parsed.
     * @return parsed object.
     */
    private RaceTime parseStringToRaceTime(String timeString) {
        String[] timeArray = timeString.split("[-,.]");
        int hours = Integer.parseInt(timeArray[0]);
        int mins = Integer.parseInt(timeArray[1]);
        int secs = Integer.parseInt(timeArray[2]);
        int mscs = Integer.parseInt(timeArray[3]);
        RaceTime raceTime;
        if (hours == 0 && mins == 0 && secs == 0 && mscs == 0) {
            raceTime = new RaceTime(false, hours, mins, secs, mscs);
        } else {
            raceTime = new RaceTime(true, hours, mins, secs, mscs);
        }
        return raceTime;
    }

    // TODO: 13.08.2019 добавить валидацию на сервере >0 итд
    // see main.java.application.rally.validator

    // TODO: 13.08.2019 тег а кэширование не в нем
    // see main.webapp.filter.CacheDisableFilter

    @Override
    public void doEditResult(HttpServletRequest request, HttpServletResponse response)
            throws AddDataDaoErrorException, org.json.simple.parser.ParseException {
        try {
            RallyDao rallyDAO = new DatabaseRallyDao();

            String jsonData = request.getParameter(AttributeName.DATA);
            JSONArray jsonArray = (JSONArray) new JSONParser().parse(jsonData);

            for (Object o : jsonArray) {
                JSONObject object = (JSONObject) o;

                int teamId = Integer.parseInt((String) object.get(JsonFiledName.JsonResult.TEAM_ID));
                if (teamId < 0) throw new IllegalArgumentException("Team ID can't ne negative.");
                int eventId = Integer.parseInt((String) object.get(JsonFiledName.JsonResult.ID_EVENT));
                if (eventId < 0) throw new IllegalArgumentException("Event ID can't be negative.");

                String timeString = (String) object.get(JsonFiledName.JsonResult.RESULT_TIME);
                RaceTime raceTime = parseStringToRaceTime(timeString);
                new RaceTimeValidator().validate(raceTime);

                rallyDAO.addResultByParameters(eventId, raceTime.getCalculatedResult(), teamId);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    @Override
    public void gotoParticipatePage(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void doParticipate(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }
}
