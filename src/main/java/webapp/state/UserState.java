package webapp.state;

import application.rally.Event;
import application.rally.Pilot;
import application.rally.Team;
import application.rally.validator.PilotValidator;
import application.rally.validator.TeamValidator;
import application.user.User;
import core.factory.PageType;
import data.rallydao.RallyDao;
import data.rallydao.exception.AddDataDaoErrorException;
import data.rallydao.exception.DataCollectionDaoErrorException;
import data.rallydao.exception.DeleteDataDaoErrorException;
import data.rallydao.mysql.DatabaseRallyDao;
import data.userdao.UserDao;
import data.userdao.exception.AddUserDaoErrorException;
import data.userdao.exception.GetUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;
import data.userdao.mysql.DatabaseUserDao;
import webapp.AttributeName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class UserState implements State {
    private static final Logger logger = LogManager.getLogger(UserState.class);

    @Override
    public void doRegistration(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoCabinet(HttpServletRequest request, HttpServletResponse response) throws GetUserDaoErrorException, AccessDeniedException {
        try {
            int userId = Integer.parseInt((String) request.getSession().getAttribute(AttributeName.ID));
            UserDao userDao = new DatabaseUserDao();
            User user = userDao.getById(userId);

            RallyDao rallyDAO = new DatabaseRallyDao();
            if (user.getIdMyTeam() != null && user.getIdMyTeam() != 0) {
                Team team = rallyDAO.getTeamById(user.getIdMyTeam());
                request.setAttribute(AttributeName.USERS_TEAM, team);
            }
            request.getRequestDispatcher(PageType.getPage(PageType.USER_MENU)).forward(request, response);
        } catch (Exception e) {
            logger.error(e);
            throw new GetUserDaoErrorException(e);

        }
    }

    @Override
    public void doLogout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        removeUserAttributes(request, response);
        request.getRequestDispatcher(PageType.getPage(PageType.INDEX)).forward(request, response);
    }


    @Override
    public void doDeleteUser(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);

    }

    @Override
    public void gotoUsersList(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    private void addPilotsToDao(final Pilot driver, final Pilot codriver) throws AddDataDaoErrorException {
        RallyDao rallyDAO = new DatabaseRallyDao();

        PilotValidator pilotValidator = new PilotValidator();
        pilotValidator.validate(driver);
        pilotValidator.validate(codriver);

        rallyDAO.addPilotAndSetCorrectID(driver);
        rallyDAO.addPilotAndSetCorrectID(codriver);
    }

    private Team addTeamFromRequestToDao(HttpServletRequest request) throws AddDataDaoErrorException {
        // adding pilots
        Pilot driver = new Pilot(request.getParameter(AttributeName.NEW_DRIVER), 0);
        Pilot codriver = new Pilot(request.getParameter(AttributeName.NEW_CODRIVER), 0);
        addPilotsToDao(driver, codriver);

        // creating a new team
        Integer carNumber = Integer.valueOf(request.getParameter(AttributeName.NEW_CAR_NUMBER));
        String car = request.getParameter(AttributeName.NEW_CAR);
        Team team = new Team(0, driver, codriver, carNumber, car);
        new TeamValidator().validate(team);
        RallyDao rallyDao = new DatabaseRallyDao();
        team = rallyDao.addTeamAndSetCorrectID(team);
        return team;
    }

    private void addUsersTeamToDao(Team team, int userId) throws NoSuchUserException, GetUserDaoErrorException, AddUserDaoErrorException {
        UserDao userDao = new DatabaseUserDao();
        User oldUser = userDao.getById(userId);
        User newUser = new User(oldUser.getId(), oldUser.getLogin(), oldUser.getPasswordHash(), oldUser.getRole(), oldUser.getSalt(), team.getId());
        userDao.alterUser(oldUser.getId(), newUser);
    }


    @Override
    public void doCreateTeam(HttpServletRequest request, HttpServletResponse response) throws AddUserDaoErrorException {
        logger.trace("Creating team linked to the user...");

        try {
            Team team = addTeamFromRequestToDao(request);
            int userId = Integer.parseInt((String) request.getSession().getAttribute(AttributeName.ID));
            addUsersTeamToDao(team, userId);
        } catch (GetUserDaoErrorException | NoSuchUserException | AddDataDaoErrorException | AddUserDaoErrorException e) {
            e.printStackTrace();
            logger.error(e);
            throw new AddUserDaoErrorException(e);
        }
        logger.debug("Team has been created and connected to the user.");
    }

    @Override
    public void gotoTeamCreation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(PageType.getPage(PageType.GOTO_CREATION_TEAM)).forward(request, response);
    }

    @Override
    public void gotoEventCreation(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void doAddEvent(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoResultEditing(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void doEditResult(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoParticipatePage(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DataCollectionDaoErrorException, GetUserDaoErrorException, NoSuchUserException {

        try {
            User user = new DatabaseUserDao().getById(Integer.parseInt((String) request.getSession().getAttribute(AttributeName.ID)));

            RallyDao rallyDAO = new DatabaseRallyDao();
            ArrayList<Event> events = rallyDAO.getEvents();
            ArrayList<Event> openEvents = new ArrayList<>();
            for (Event event : events) {
                if (event.isOpen()) {
                    openEvents.add(event);
                }
            }

            ArrayList<Event> eventsWithThisUser = rallyDAO.getEventsWithTeamIdParticipated(user.getIdMyTeam());
            request.setAttribute(AttributeName.EVENTS, openEvents);
            request.setAttribute(AttributeName.EVENTS_WITH_THIS_USER, eventsWithThisUser);
            request.getRequestDispatcher(PageType.getPage(PageType.GOTO_PARTICIPATE)).forward(request, response);

        } catch (DataCollectionDaoErrorException | ServletException | IOException | GetUserDaoErrorException | NoSuchUserException e) {
            logger.error(e);
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void doParticipate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            User user = new DatabaseUserDao().getById(Integer.parseInt((String) request.getSession().getAttribute(AttributeName.ID)));

            if (user.getIdMyTeam() == null) {
                throw new IllegalArgumentException("User hasn't registered a team.");
            }

            String data = request.getParameter(AttributeName.PARTICIPATE_DATA);
            JSONArray array = (JSONArray) new JSONParser().parse(data);
            Iterator arrayItr = array.iterator();
            RallyDao rallyDAO = new DatabaseRallyDao();
            while (arrayItr.hasNext()) {
                JSONObject object = (JSONObject) arrayItr.next();
                int idEvent = Integer.parseInt(object.get("id").toString());
                boolean isParticipate = Boolean.parseBoolean(object.get("value").toString());

                if (isParticipate) {
                    rallyDAO.addResultByParameters(idEvent, 0, user.getIdMyTeam());
                } else {
                    rallyDAO.deleteResultByTeamIdEventId(user.getIdMyTeam(), idEvent);
                }
            }
        } catch (ParseException | GetUserDaoErrorException | NoSuchUserException | AddDataDaoErrorException | DeleteDataDaoErrorException e) {
            e.printStackTrace();
            logger.error(e);
            throw new Exception(e);
        }
    }
}
