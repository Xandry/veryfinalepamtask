package webapp.state;

public class AccessDeniedException extends Exception {
    public static final String MESSAGE = "You have no access to this action.";

    public AccessDeniedException() {
    }

    public AccessDeniedException(String message) {
        super(message);
    }

    public AccessDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccessDeniedException(Throwable cause) {
        super(cause);
    }
}
