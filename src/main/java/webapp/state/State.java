package webapp.state;

import data.rallydao.exception.AddDataDaoErrorException;
import data.rallydao.exception.DataCollectionDaoErrorException;
import data.userdao.exception.AddUserDaoErrorException;
import data.userdao.exception.DeleteUserDaoErrorException;
import data.userdao.exception.GetUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;
import security.HashingAlgorithmException;
import webapp.AttributeName;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This interface this basic interface for the 'State' pattern.
 * Every implementation may have different access level
 * (e.g. Admin, User, Unknown)
 *
 */
public interface State {
    /**
     * Creates a new user using data out of request.
     * Must be implemented for unauthorized clients.
     *
     * @param request  request to the server that contains login and password of the new client.
     * @param response response of the server.
     * @throws AddUserDaoErrorException  in case of an error of Dao layer.
     * @throws HashingAlgorithmException error of creating new hash and salt pair.
     * @throws ServletException          exception of forwarding of client.
     * @throws IOException               if any I/O error occupies.
     * @throws AccessDeniedException     if already registered client tries to register again.
     */
    void doRegistration(HttpServletRequest request, HttpServletResponse response)
            throws AddUserDaoErrorException, HashingAlgorithmException, ServletException, IOException, AccessDeniedException;

    /**
     * Forwards a client to the a cabinet according to its role.
     * Must be forbidden for unauthorized clients.
     *
     * @param request  request ot the server with session that contains client's data.
     * @param response response of the server.
     * @throws ServletException                exception of forwarding of client.
     * @throws IOException                     if any I/O error occupies.
     * @throws GetUserDaoErrorException        in case of an error of Dao layer.
     * @throws NoSuchUserException             if there is no user with as data as in the Dao.
     * @throws DataCollectionDaoErrorException in case of an error of Dao layer.
     * @throws AccessDeniedException           if unauthorized client tries to get access.
     */
    void gotoCabinet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, GetUserDaoErrorException, NoSuchUserException, DataCollectionDaoErrorException, AccessDeniedException;

    /**
     * Deletes client's data out of session and force the client authorize again if he
     * would like to get access to a cabinet and others features as well.
     * After this forwards client to the index page.
     *
     * @param request  request ot the server with session that contains client's data.
     * @param response response of the server.
     * @throws ServletException      exception of forwarding of client.
     * @throws IOException           if any I/O error occupies.
     * @throws AccessDeniedException if unauthorized client tries to logout
     */
    void doLogout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, AccessDeniedException;


    /**
     * Deletes user out of Dao.
     *
     * @param request  contains id of user going to be deleted.
     * @param response response of the server.
     * @throws DeleteUserDaoErrorException in case of an error of User Dao layer.
     * @throws ServletException            exception of forwarding of client.
     * @throws IOException                 if any I/O error occupies.
     * @throws GetUserDaoErrorException    in case of an error of User Dao layer.
     * @throws NoSuchUserException         if there is no user with as data as in the Dao.
     * @throws AccessDeniedException       if unauthorized client tries to get access.
     */
    void doDeleteUser(HttpServletRequest request, HttpServletResponse response)
            throws DeleteUserDaoErrorException, ServletException, IOException, GetUserDaoErrorException, NoSuchUserException, AccessDeniedException;

    /**
     * Forwards the client to page that contains list of all users.
     * Client can delete them by sending requests to the server.
     *
     * @param request  request that contains list of all users without their hashes.
     * @param response response of the server.
     * @throws GetUserDaoErrorException in case of an error of User Dao layer.
     * @throws ServletException         exception of forwarding of client.
     * @throws IOException              if any I/O error occupies.
     * @throws AccessDeniedException    if unauthorized client tries to get access.
     */
    void gotoUsersList(HttpServletRequest request, HttpServletResponse response)
            throws GetUserDaoErrorException, ServletException, IOException, AccessDeniedException;


    /**
     * Users can create a team to participate in an open competitions.
     * This method creates a team on Dao and connect the user with the created team.
     *
     * @param request  request to the server that contains team's data.
     * @param response response of the server.
     * @throws AddDataDaoErrorException in case of an error of Rally Dao layer.
     * @throws GetUserDaoErrorException in case of an error of Rally Dao layer.
     * @throws NoSuchUserException      if there is no user with as data as in the Dao.
     * @throws AddUserDaoErrorException in case of an error of User Dao layer.
     * @throws IOException              if any I/O error occupies.
     * @throws AccessDeniedException    if unauthorized client tries to get access.
     */
    void doCreateTeam(HttpServletRequest request, HttpServletResponse response)
            throws AddDataDaoErrorException, GetUserDaoErrorException, NoSuchUserException, AddUserDaoErrorException, IOException, AccessDeniedException;

    /**
     * Forwards the client to the page where it can create a new team to
     * participate in open competitions.
     *
     * @param request  request to the server.
     * @param response response of the server.
     * @throws ServletException      exception of forwarding of the client.
     * @throws IOException           if any I/O error occupies.
     * @throws AccessDeniedException if unauthorized client tries to get access.
     */
    void gotoTeamCreation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, AccessDeniedException;

    /**
     * Forwards the client to the page where it can create a new event.
     *
     * @param request  request to the server.
     * @param response respinse of the server.
     * @throws ServletException      exception of forwarding of the client.
     * @throws IOException           if any I/O error occupies.
     * @throws AccessDeniedException if unauthorized client tries to get access.
     */
    void gotoEventCreation(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, AccessDeniedException;

    /**
     * Users can create a new event, whether open or not.
     *
     * @param request  request to the server with event's data.
     * @param response response of the server.
     * @throws AddDataDaoErrorException in case of an error of Rally Dao layer.
     * @throws IOException              if any I/O error occupies.
     * @throws AccessDeniedException    if unauthorized client tries to get access.
     */
    void doAddEvent(HttpServletRequest request, HttpServletResponse response)
            throws AddDataDaoErrorException, IOException, AccessDeniedException;

    /**
     * Forwards the user to the page where he can creates result of existing teams on
     * existing events.
     *
     * @param request  request to the server.
     * @param response response of the server.
     * @throws DataCollectionDaoErrorException in case of an error of Rally Dao layer.
     * @throws IOException                     if any I/O error occupies.
     * @throws ServletException                exception of forwarding of the client.
     * @throws AccessDeniedException           if unauthorized client tries to get access.
     */
    void gotoResultEditing(HttpServletRequest request, HttpServletResponse response)
            throws DataCollectionDaoErrorException, IOException, ServletException, AccessDeniedException;

    /**
     * Client can create result of existing teams on
     * existing events. This method receive Json string from the
     * client. This Json contains array of object, which of them contains
     * team, event and time.
     *
     * @param request  request to the server with the Json data.
     * @param response response of the server.
     * @throws AddDataDaoErrorException in case of an error of Rally Dao layer.
     * @throws IOException              if any I/O error occupies.
     * @throws AccessDeniedException    if unauthorized client tries to get access.
     * @throws ParseException           an error during Json parsing.
     */
    void doEditResult(HttpServletRequest request, HttpServletResponse response)
            throws AddDataDaoErrorException, IOException, AccessDeniedException, ParseException;

    /**
     * Forwards the user to the page where it can select events it wants to join.
     *
     * @param request  request to the server.
     * @param response response of the server.
     * @throws ServletException                exception of forwarding of the client.
     * @throws IOException                     if any I/O error occupies.
     * @throws DataCollectionDaoErrorException in case of an error of Rally Dao layer.
     * @throws GetUserDaoErrorException        in case of an error of User Dao layer.
     * @throws NoSuchUserException             if there is no user with as data as in the Dao.
     * @throws AccessDeniedException           if unauthorized client tries to get access.
     */
    void gotoParticipatePage(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DataCollectionDaoErrorException, GetUserDaoErrorException, NoSuchUserException, AccessDeniedException;

    /**
     * User a with registered team can select open events ghe wants to join.
     * This method receive Json array of object each of them contains
     * id of a event and boolean variable that means if the user wants to join the
     * competition or not. Then method changes Rally Dao according to user's involves.
     *
     * @param request request to the server with Json string.
     * @param response response of the server.
     * @throws Exception any error occupies.
     */
    void doParticipate(HttpServletRequest request, HttpServletResponse response)
            throws Exception;


    /**
     * Delete user's data out of request and its session.
     *
     * @param request  request to be cleaned.
     * @param response response.
     */
    default void removeUserAttributes(HttpServletRequest request, HttpServletResponse response) {
        request.removeAttribute(AttributeName.LOGIN);
        request.removeAttribute(AttributeName.PASSWORD);
        request.removeAttribute(AttributeName.ROLE);
        request.getSession().removeAttribute(AttributeName.LOGIN);
        request.getSession().removeAttribute(AttributeName.PASSWORD);
        request.getSession().removeAttribute(AttributeName.ROLE);
    }
}