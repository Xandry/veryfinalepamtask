package webapp.state;

import application.user.User;
import application.user.UserRole;
import core.factory.PageType;
import data.userdao.UserDao;
import data.userdao.exception.AddUserDaoErrorException;
import data.userdao.exception.NoSuchUserException;
import data.userdao.mysql.DatabaseUserDao;
import security.HashSaltPair;
import security.Hasher;
import security.HashingAlgorithmException;
import webapp.AttributeName;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UnknownState implements State {
    @Override
    public void doRegistration(HttpServletRequest request, HttpServletResponse response) throws AddUserDaoErrorException, HashingAlgorithmException, ServletException, IOException {
        String login = request.getParameter(AttributeName.NEW_LOGIN);
        String password = request.getParameter(AttributeName.NEW_PASSWORD);

        Hasher hasher = new Hasher();
        HashSaltPair pair = hasher.createHashAndSalt(password);
        String salt = pair.getSalt();
        String hash = pair.getHash();

        int id = 0;     // user's id doesn't matter now
        User user = new User(id, login, hash, UserRole.USER, salt, null);

        UserDao usersHandler = new DatabaseUserDao();
        usersHandler.addUser(user);
        request.getRequestDispatcher(PageType.getPage(PageType.INDEX)).forward(request, response);
    }

    @Override
    public void gotoCabinet(HttpServletRequest request, HttpServletResponse response) throws NoSuchUserException {
        throw new NoSuchUserException("No such user");

    }

    @Override
    public void doLogout(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void doDeleteUser(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoUsersList(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void doCreateTeam(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoTeamCreation(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoEventCreation(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void doAddEvent(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoResultEditing(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void doEditResult(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }

    @Override
    public void gotoParticipatePage(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);

    }

    @Override
    public void doParticipate(HttpServletRequest request, HttpServletResponse response) throws AccessDeniedException {
        throw new AccessDeniedException(AccessDeniedException.MESSAGE);
    }
}
