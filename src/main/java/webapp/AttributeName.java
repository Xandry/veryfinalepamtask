package webapp;

public class AttributeName {
    public static final String EXCEPTION = "exception";
    public static final String LANGUAGE = "language";

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ROLE = "role";

    public static final String NEW_LOGIN = "newlogin";
    public static final String NEW_PASSWORD = "newpassword";
    public static final String NEW_DRIVER = "new_driver";
    public static final String NEW_CODRIVER = "new_codriver";
    public static final String NEW_CAR = "new_car";
    public static final String NEW_CAR_NUMBER = "new_car_number";

    public static final String NEW_EVENT_NAME = "new_event_name";
    public static final String NEW_EVENT_DESCRIPTION = "new_event_description";
    public static final String NEW_EVENT_COUNTRY = "new_event_country";
    public static final String NEW_EVENT_IS_OPEN = "new_event_is_open";
    public static final String NEW_EVENT_START_DATE = "new_event_start_date";
    public static final String NEW_EVENT_END_DATE = "new_event_end_date";

    public static final String PAGE = "page";

    public static final String EVENT = "event";
    public static final String EVENTS = "events";
    public static final String EVENTS_WITH_THIS_USER = "events_with_this_user";
    public static final String RESULTS = "results";
    public static final String USERS = "users";
    public static final String TEAMS = "teams";

    public static final String ID = "id";
    public static final String STATE = "state";
    public static final String USERS_TEAM = "users_team";

    public static final String PARTICIPATE_DATA = "participate_data";
    public static final String DATA = "data";
}
