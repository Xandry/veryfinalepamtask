package webapp.servlet;

import data.connectionpool.ConnectionException;
import data.connectionpool.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.mysql.ErrorReadingPropertiesFileException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Listener implements ServletContextListener {
    private static final Logger logger = LogManager.getLogger(Listener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            ConnectionPool.init();
        } catch (ErrorReadingPropertiesFileException e) {
            logger.fatal(e + " while servlet context initialization.");
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            ConnectionPool.getConnectionPool().closeAll();
        } catch (ConnectionException | ErrorReadingPropertiesFileException e) {
            logger.fatal(e + " while servlet context destroying.");
        }
    }

}
