package webapp.servlet;

import core.factory.Director;
import core.factory.PageDirectorFactory;
import core.factory.PageType;
import core.factory.index.IndexDirector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import webapp.AttributeName;
import webapp.state.State;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlet")
public class Servlet extends HttpServlet {
    private static final String PAGE_PARAMETER = AttributeName.PAGE;
    private static final Logger logger = LogManager.getLogger(Servlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            new IndexDirector().handle(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            logger.error(e);
            try {
                resp.sendError(400);
            } catch (IOException ex) {
                logger.fatal(ex);
                ex.printStackTrace();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        PageType pageType = PageType.valueOf(req.getParameter(PAGE_PARAMETER));
        State state = (State) req.getAttribute(AttributeName.STATE);
        try {
            switch (pageType) {
                case REGISTRATION:
                    state.doRegistration(req, resp);
                    return;
                case CABINET:
                    state.gotoCabinet(req, resp);
                    return;
                case LOGOUT:
                    state.doLogout(req, resp);
                    return;
                case ALL_USERS:
                    state.gotoUsersList(req, resp);
                    return;
                case DELETE_USER:
                    state.doDeleteUser(req, resp);
                    return;
                case CREATE_TEAM:
                    state.doCreateTeam(req, resp);
                    return;
                case GOTO_CREATION_TEAM:
                    state.gotoTeamCreation(req, resp);
                    return;
                case GOTO_EVENT_CREATION:
                    state.gotoEventCreation(req, resp);
                    return;
                case CREATE_EVENT:
                    state.doAddEvent(req, resp);
                    return;
                case GOTO_PARTICIPATE:
                    state.gotoParticipatePage(req, resp);
                    return;
                case PARTICIPATE:
                    state.doParticipate(req, resp);
                    return;
                case GOTO_EDIT_RESULT:
                    state.gotoResultEditing(req, resp);
                    return;
                case EDIT_RESULT:
                    state.doEditResult(req, resp);
                    return;
                default:
                    Director director = PageDirectorFactory.createPageDirector(pageType);
                    director.handle(req, resp);
            }
        } catch (Exception e) {
            forwardToErrorPage(e, req, resp);
            logger.error(e + " in doPost() method");
        }
    }

    private void forwardToErrorPage(Exception e, HttpServletRequest req, HttpServletResponse resp) {
        try {
            req.setAttribute(AttributeName.EXCEPTION, e);
            req.getRequestDispatcher(PageType.getPage(PageType.ERROR)).forward(req, resp);
        } catch (IOException | ServletException exception) {
            logger.fatal(exception + " while forwarding to error page");
        }
    }

    // TODO: 13.08.2019 зарегистрировать лисенер и перенести это туда (при старте томката)
    // see class Listener
}
