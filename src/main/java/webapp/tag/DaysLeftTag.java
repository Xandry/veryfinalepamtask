package webapp.tag;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class DaysLeftTag extends TagSupport {
    private String date;

    private long delta;

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int doStartTag() {
        try {
            long millisEventStart = new SimpleDateFormat("yyyy-MM-dd").parse(date).getTime();
            GregorianCalendar calendar = new GregorianCalendar();
            long millisNow = calendar.getTimeInMillis();

            delta = millisEventStart - millisNow;
            int days;
            if (delta > 0) {
                days = (int) (delta / (1000 * 60 * 60 * 24));
                JspWriter writer = pageContext.getOut();
                writer.print(days);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        if (delta > 0)
            return EVAL_BODY_INCLUDE;
        else
            return SKIP_BODY;
    }
}
