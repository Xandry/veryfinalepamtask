package util.mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MysqlUtil {
    private static final Logger logger = LogManager.getLogger(MysqlUtil.class);

    public static ResultSet makeQuery(Connection connection, String sqlQuery, ArrayList<String> params) throws SQLException {
        PreparedStatement statement;
        ResultSet resultSet;
        try {
            statement = connection.prepareStatement(sqlQuery);
            int index = 1;
            for (String parameter : params) {
                statement.setObject(index++, parameter);
            }
            resultSet = statement.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            logger.error(e);
            throw e;
        }
    }
}
