package util.mysql.rallyDB.columnName;

public enum TeamColumnName {
    ID("idTeam"),
    CAR_NUMBER("CarNumber"),
    CAR("Car"),
    DRIVER_ID("idDriver"),
    CODRIVER_ID("idCodriver");

    private String columnName;

    TeamColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
