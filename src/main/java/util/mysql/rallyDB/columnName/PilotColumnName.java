package util.mysql.rallyDB.columnName;

public enum PilotColumnName {
    ID("idPilot"),
    NAME("Name");


    private String columnName;

    PilotColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
