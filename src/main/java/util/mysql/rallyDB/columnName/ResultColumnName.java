package util.mysql.rallyDB.columnName;

public enum ResultColumnName {
    EVENT_ID("idEvent"),
    TIME("Time"),
    IS_FINISHED("isFinished"),
    TEAM_ID("idTeam");


    private String columnName;

    ResultColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
