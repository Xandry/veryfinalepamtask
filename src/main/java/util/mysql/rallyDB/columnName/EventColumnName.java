package util.mysql.rallyDB.columnName;

public enum EventColumnName {
    ID("idEvent"),
    NAME("Name"),
    DESCRIPTION("Description"),
    COUNTRY("Country"),
    IS_OPEN("isOpen"),
    START_DATE("StartDate"),
    END_DATE("EndDate");


    private String columnName;

    EventColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
