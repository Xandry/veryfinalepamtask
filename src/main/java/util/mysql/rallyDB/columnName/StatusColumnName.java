package util.mysql.rallyDB.columnName;

public enum StatusColumnName {
    ID("idStatus"),
    DESCRIPTION("Description");

    private String columnName;

    StatusColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
