package util.mysql.rallyDB.columnName;

public enum UserColumnName {
    ID("idUser"),
    LOGIN("Login"),
    PASSWORD_HASH("PasswordHash"),
    PASSWORD_SALT("Salt"),
    STATUS_ID("idStatus"),
    MY_TEAM_ID("idMyTeam");


    private String columnName;

    UserColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
