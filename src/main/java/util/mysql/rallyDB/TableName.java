package util.mysql.rallyDB;

public enum TableName {
    EVENT("event"),
    PILOT("pilot"),
    RESULT("result"),
    STATUS("status"),
    TEAM("team"),
    USER("user");

    private String tableName;

    TableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName() {
        return tableName;
    }
}
