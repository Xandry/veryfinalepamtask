package util.mysql;

public class ErrorReadingPropertiesFileException extends Exception {
    public ErrorReadingPropertiesFileException() {
    }

    public ErrorReadingPropertiesFileException(String message) {
        super(message);
    }

    public ErrorReadingPropertiesFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorReadingPropertiesFileException(Throwable cause) {
        super(cause);
    }
}
