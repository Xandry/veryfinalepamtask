package util.mysql;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class MysqlPropertiesFileHandler {
    private static final String FILENAME = "src\\main\\resources\\mysql_settings.prop";
    private static final String URL = "URL";
    private static final String DRIVER = "Driver";
    private static final String LOGIN = "Login";
    private static final String PASSWORD = "Password";

    private String driver;
    private String login;
    private String password;
    private String url;

    public MysqlPropertiesFileHandler() throws ErrorReadingPropertiesFileException {
        load();
    }

    public String getDriver() {
        return driver;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }

    private void load() throws ErrorReadingPropertiesFileException {
        FileInputStream fis;
        try {
            fis = new FileInputStream(FILENAME);
            Properties p = new Properties();
            p.load(fis);
            driver = (String) p.get(DRIVER);
            url = (String) p.get(URL);
            login = (String) p.get(LOGIN);
            password = (String) p.get(PASSWORD);
        } catch (IOException e) {
//            e.printStackTrace();
            throw new ErrorReadingPropertiesFileException(e);
        }


    }
}
