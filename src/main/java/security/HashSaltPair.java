package security;

import java.util.Objects;

/**
 * Contain a generated hash and salt for an user.
 */
public class HashSaltPair {
    private String hash;
    private String salt;

    public HashSaltPair(String hash, String salt) {
        this.hash = hash;
        this.salt = salt;
    }

    public String getHash() {
        return hash;
    }

    public String getSalt() {
        return salt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HashSaltPair pair = (HashSaltPair) o;
        return hash.equals(pair.hash) &&
                salt.equals(pair.salt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash, salt);
    }
}