package security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Properties;

public class Hasher {
    private static final Logger logger = LogManager.getLogger(Hasher.class);

    private static final String FILENAME = "src/main/resources/hashing_settings.prop";
    private static final String HASH_SIZE_FIELD = "HashSize";
    private static final String SALT_SIZE_FIELD = "SaltSize";
    private static final String COMPLEXITY_FIELD = "Complexity";
    private static final String ALGORITHM_FIELD = "Algorithm";

    private static int hashSize;
    private static int saltSize;
    private static int algorithmComplexity;
    private static String algorithm = null;

    public Hasher() throws HashingAlgorithmException {
        if (algorithm == null) {
            init();
        }
    }

    public static int getHashSize() {
        return hashSize;
    }

    public static int getSaltSize() {
        return saltSize;
    }

    public static int getAlgorithmComplexity() {
        return algorithmComplexity;
    }

    public static String getAlgorithm() {
        return algorithm;
    }

    /**
     * Generates a new salt with set parameters.
     *
     * @return new salt.
     */
    public static String getNewSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[saltSize];
        random.nextBytes(salt);
        return new String(salt);
    }

    private void init() throws HashingAlgorithmException {
        try {
            logger.trace("Reading hash property file...");
            FileInputStream fis = new FileInputStream(FILENAME);
            Properties p = new Properties();
            p.load(fis);
            hashSize = Integer.valueOf((String) p.get(HASH_SIZE_FIELD));
            saltSize = Integer.valueOf((String) p.get(SALT_SIZE_FIELD));
            algorithmComplexity = Integer.valueOf((String) p.get(COMPLEXITY_FIELD));
            algorithm = (String) p.get(ALGORITHM_FIELD);
            logger.debug("Hashing .prop file has been successfully read.");
        } catch (IOException | ClassCastException | NullPointerException e) {
            logger.fatal(e + " while reading hashing .prop settings file.");
            e.printStackTrace();
            throw new HashingAlgorithmException(e);
        }
    }

    /**
     * Gets hash by existing salt.
     *
     * @param password - password to be hashed.
     * @param salt     - salt to hashing algorithm.
     * @return - hash of the password.
     * @throws HashingAlgorithmException if hashing algorithm was configured incorrect.
     */
    public String getHash(String password, String salt) throws HashingAlgorithmException {
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), algorithmComplexity, hashSize);
            SecretKeyFactory factory = SecretKeyFactory.getInstance(algorithm);
            return new String(factory.generateSecret(spec).getEncoded());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            logger.fatal(e);
            throw new HashingAlgorithmException(e);
        }
    }

    /**
     * This method hashes password with new salt.
     *
     * @param password - password to be hashed.
     * @return Pair of two element - hash and salt.
     * @throws HashingAlgorithmException if hashing algorithm was configured incorrect.
     * @see HashSaltPair
     * @see Hasher
     */
    public HashSaltPair createHashAndSalt(String password) throws HashingAlgorithmException {
        try {
            String salt = getNewSalt();
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), algorithmComplexity, hashSize);
            SecretKeyFactory factory = SecretKeyFactory.getInstance(algorithm);
            String hash = new String(factory.generateSecret(spec).getEncoded());
            return new HashSaltPair(hash, salt);

        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            logger.fatal(e);
            throw new HashingAlgorithmException(e);
        }
    }
}
