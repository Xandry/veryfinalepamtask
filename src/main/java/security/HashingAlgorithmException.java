package security;

public class HashingAlgorithmException extends Exception {
    public HashingAlgorithmException() {
    }

    public HashingAlgorithmException(String message) {
        super(message);
    }

    public HashingAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }

    public HashingAlgorithmException(Throwable cause) {
        super(cause);
    }
}
