package application.user;

/**
 * Roles of website's users.
 */
public enum UserRole {
    ADMIN,
    USER,
    UNKNOWN;

    public static UserRole parseInt(int i) {
        switch (i) {
            case 0:
                return ADMIN;
            case 1:
                return USER;
            default:
                return UNKNOWN;
        }
    }

    public static int toInt(UserRole userRole) {
        switch (userRole) {
            case ADMIN:
                return 0;
            case USER:
                return 1;
            default:
                return 2;
        }
    }
}
