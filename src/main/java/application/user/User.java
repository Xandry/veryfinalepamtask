package application.user;

import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class that represents account on website.
 * Has id, login, password hash and its salt, and role.
 *
 * @see UserRole
 */
public class User implements Serializable {
    private int id;

    private String login;

    private String passwordHash;

    private UserRole role;

    private String salt;

    private Integer idMyTeam;

    public User(int id, String login, String passwordHash, UserRole role, String salt, Integer idMyTeam) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
        this.salt = salt;
        this.idMyTeam = idMyTeam;
    }

    public Integer getIdMyTeam() {
        return idMyTeam;
    }

    public String getSalt() {
        return salt;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public UserRole getRole() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                login.equals(user.login) &&
                passwordHash.equals(user.passwordHash) &&
                role == user.role &&
                salt.equals(user.salt) &&
                Objects.equals(idMyTeam, user.idMyTeam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, passwordHash, role, salt, idMyTeam);
    }
}
