package application.resultcomparator;

import application.rally.Result;

import java.util.Comparator;

/**
 * Class that implements {@code Comparator}.
 */
public class ResultTimeComparator implements Comparator<Result> {
    /**
     * Compares two results.
     * Result that has {@code ifFinished == true} always more
     * than result with {@code isFinished == false}.
     *
     * @param o1 first result.
     * @param o2 second result.
     * @return result of comparison.
     */
    @Override
    public int compare(Result o1, Result o2) {
        if (o1.getTime().isFinished() != o2.getTime().isFinished()) {
            return Boolean.compare(o1.getTime().isFinished(), o2.getTime().isFinished()) * -1;
        }
        return Integer.compare(o1.getTime().getCalculatedResult(), o2.getTime().getCalculatedResult());
    }
}
