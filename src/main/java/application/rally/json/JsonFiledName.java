package application.rally.json;

public class JsonFiledName {

    public static class JsonResult {
        public static final String ID_EVENT = "eventId";
        public static final String RESULT_TIME = "resultTime";
        public static final String TEAM_ID = "teamId";
    }

    public static class JsonRaceTime {
        public static final String HOURS = "hours";
        public static final String MINUTES = "minutes";
        public static final String SECONDS = "seconds";
        public static final String MILLISECONDS = "milliseconds";
        public static final String IS_FINISHED = "isFinished";
    }

    public static class JsonEvent {
        public static final String EVENT_ID = "eventId";
        public static final String EVENT_NAME = "eventName";
        public static final String EVENT_DESCRIPTION = "eventDescription";
        public static final String EVENT_COUNTRY = "eventCountry";
        public static final String EVENT_IS_OPEN = "eventIsOpen";
        public static final String EVENT_START_DATE = "eventStartDate";
        public static final String EVENT_END_DATE = "eventEndDate";
    }
}

