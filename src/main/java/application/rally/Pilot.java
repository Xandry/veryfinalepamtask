package application.rally;

import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class that represents pilot - driver or codriver.
 */
public class Pilot implements Serializable {
    private String name;
    private int idPilot;

    public Pilot(String name, int idPilot) {
        this.name = name;
        this.idPilot = idPilot;
    }

    public String getName() {
        return name;
    }

    public int getIdPilot() {
        return idPilot;
    }

    public void setIdPilot(int idPilot) {
        this.idPilot = idPilot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pilot pilot = (Pilot) o;
        return idPilot == pilot.idPilot &&
                name.equals(pilot.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, idPilot);
    }
}
