package application.rally.validator;

import application.rally.Result;

public class ResultValidator {
    public void validate(Result result) {
        if (result.getIdEvent() < 0) {
            throw new IllegalArgumentException("Event ID can't be negative");
        }
        new RaceTimeValidator().validate(result.getTime());
        new TeamValidator().validate(result.getTeam());

    }
}
