package application.rally.validator;


import application.rally.RaceTime;

public class RaceTimeValidator {
    public void validate(RaceTime raceTime) {
        if (raceTime.getHours() < 0) {
            throw new IllegalArgumentException("Hours number can't be negative.");
        }
        if (raceTime.getMinutes() < 0) {
            throw new IllegalArgumentException("Minutes number can't be negative.");
        }
        if (raceTime.getSeconds() < 0) {
            throw new IllegalArgumentException("Seconds number can't be negative.");
        }
        if (raceTime.getMilliseconds() < 0) {
            throw new IllegalArgumentException("Milliseconds number can't be negative.");
        }
    }
}
