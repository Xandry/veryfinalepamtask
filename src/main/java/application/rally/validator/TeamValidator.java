package application.rally.validator;

import application.rally.Team;

import java.util.regex.Pattern;

public class TeamValidator {
    public void validate(Team team) {
        if (team.getId() < 0) {
            throw new IllegalArgumentException("Team ID can't be negative.");
        }
        new PilotValidator().validate(team.getDriver());
        new PilotValidator().validate(team.getCodriver());
        if (team.getCarNumber() < 0) {
            throw new IllegalArgumentException("Car number can't be negative.");
        }
        if (!Pattern.matches("[a-zA-ZА-Яа-я0-9\\s]{2,100}", team.getCar())) {
            throw new IllegalArgumentException("Wrong car name format.");
        }
    }
}
