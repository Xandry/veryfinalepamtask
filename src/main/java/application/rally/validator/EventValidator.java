package application.rally.validator;


import application.rally.Event;

import java.util.regex.Pattern;

public class EventValidator {
    public void validate(Event event) {
        if (event.getIdEvent() < 0) {
            throw new IllegalArgumentException("Event ID can't be negative.");
        }
        if (!Pattern.matches("[a-zA-Zа-яА-Я#№\\-\\s\\d]{1,100}", event.getName())) {
            throw new IllegalArgumentException("Event's name wrong format.");
        }
        if (!Pattern.matches("[a-zA-Zа-яА-Я#№\\-\\s\\d]{1,100}", event.getDescription())) {
            throw new IllegalArgumentException("Event's description wrong format.");
        }
        if (!Pattern.matches("[a-zA-Zа-яА-Я\\s]{1,50}", event.getCountry())) {
            throw new IllegalArgumentException("Event's country wrong format.");
        }
        if (event.getStartDate() == null || event.getEndDate() == null) {
            throw new IllegalArgumentException("Date can't be null");
        }
    }
}
