package application.rally.validator;


import application.rally.Pilot;

import java.util.regex.Pattern;

public class PilotValidator {
    public void validate(Pilot pilot) {
        if (pilot.getIdPilot() < 0) {
            throw new IllegalArgumentException("Pilot's ID cannot be negative.");
        }
        if (!Pattern.matches("[a-zA-ZА-Яа-я\\s]{2,100}", pilot.getName())) {
            throw new IllegalArgumentException("Pilot's name wrong format.");
        }
    }
}
