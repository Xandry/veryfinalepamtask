package application.rally;

import com.fasterxml.jackson.annotation.JsonProperty;
import application.rally.json.JsonFiledName;

import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class that represents result of pilot and codriver in a event.
 * Can be converted to Json using Jackson library.
 */
public class Result implements Serializable {
    @JsonProperty(JsonFiledName.JsonResult.ID_EVENT)
    private int idEvent;

    @JsonProperty(JsonFiledName.JsonResult.RESULT_TIME)
    private RaceTime time;

    @JsonProperty(JsonFiledName.JsonResult.TEAM_ID)
    private Team team;

    public Result(int idEvent, RaceTime time, Team team) {
        this.idEvent = idEvent;
        this.time = time;
        this.team = team;
    }

    public int getIdEvent() {
        return idEvent;
    }

    public RaceTime getTime() {
        return time;
    }

    /**
     * @return Race time in h:m:s.ms format.
     */
    public String getTimeString() {
        return time.toString();
    }

    public Team getTeam() {
        return team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return idEvent == result.idEvent &&
                time.equals(result.time) &&
                team.equals(result.team);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEvent, time, team);
    }
}
