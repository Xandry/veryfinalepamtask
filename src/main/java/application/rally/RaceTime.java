package application.rally;

import com.fasterxml.jackson.annotation.JsonProperty;
import application.rally.json.JsonFiledName;

import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class that represents race time of a time.
 * Also has a boolean field that shows if a team has finished or it's crashed.
 * Can be converted to Json using Jackson library.
 */
public class RaceTime implements Serializable {
    @JsonProperty(JsonFiledName.JsonRaceTime.HOURS)
    private int hours;

    @JsonProperty(JsonFiledName.JsonRaceTime.MINUTES)
    private int minutes;

    @JsonProperty(JsonFiledName.JsonRaceTime.SECONDS)
    private int seconds;

    @JsonProperty(JsonFiledName.JsonRaceTime.MILLISECONDS)
    private int milliseconds;

    @JsonProperty(JsonFiledName.JsonRaceTime.IS_FINISHED)
    private boolean isFinished;

    public RaceTime(boolean isFinished, int hours, int minutes, int seconds, int milliseconds) {
        this.isFinished = isFinished;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.milliseconds = milliseconds;
    }

    /**
     * @param time Race time in milliseconds.
     */
    public RaceTime(int time) {
        this.milliseconds = 0;
        this.seconds = 0;
        this.hours = 0;
        this.minutes = 0;
        if (time == -1) {
            isFinished = false;
        } else {
            assert time > 0;
            this.milliseconds = time % 10;
            time = time / 10;
            this.seconds = time % 60;
            time = time / 60;
            this.minutes = time % 60;
            time = time / 60;
            this.hours = time;
            isFinished = true;
        }
    }

    public boolean isFinished() {
        return isFinished;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public int getMilliseconds() {
        return milliseconds;
    }

    /**
     * @return Race time in milliseconds.
     */
    public int getCalculatedResult() {
        if (!isFinished)
            return -1;
        return milliseconds + seconds * 10 + minutes * 60 * 10 + hours * 60 * 60 * 10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RaceTime raceTime = (RaceTime) o;
        return hours == raceTime.hours &&
                minutes == raceTime.minutes &&
                seconds == raceTime.seconds &&
                milliseconds == raceTime.milliseconds &&
                isFinished == raceTime.isFinished;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hours, minutes, seconds, milliseconds, isFinished);
    }

    /**
     * @return Race time in h:m:s.ms format.
     */
    @Override
    public String toString() {
        return getHours() + ":" + getMinutes() + ":" + getSeconds() + "." + getMilliseconds();
    }
}
