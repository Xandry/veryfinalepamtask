package application.rally;

import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class that contains information about team: driver, codriver, car and its number.
 */
public class Team implements Serializable {
    private int id;
    private Pilot driver;
    private Pilot codriver;
    private int carNumber;
    private String car;

    public Team(int id, Pilot driver, Pilot codriver, int carNumber, String car) {
        this.id = id;
        this.driver = driver;
        this.codriver = codriver;
        this.carNumber = carNumber;
        this.car = car;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pilot getDriver() {
        return driver;
    }

    public Pilot getCodriver() {
        return codriver;
    }

    public int getCarNumber() {
        return carNumber;
    }

    public String getCar() {
        return car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return id == team.id &&
                carNumber == team.carNumber &&
                driver.equals(team.driver) &&
                codriver.equals(team.codriver) &&
                car.equals(team.car);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, driver, codriver, carNumber, car);
    }
}
