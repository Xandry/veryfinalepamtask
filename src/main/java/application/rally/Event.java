package application.rally;

import com.fasterxml.jackson.annotation.JsonProperty;
import application.rally.json.JsonFiledName;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Entity class that represents one rally race. It has id, name, description,
 * country, boolean variable {@code isOpen} that shows if it's a open race and
 * start and end dates.
 * Can be converted to Json using Jackson library.
 */
public class Event implements Serializable {

    @JsonProperty(JsonFiledName.JsonEvent.EVENT_ID)
    private int idEvent;

    @JsonProperty(JsonFiledName.JsonEvent.EVENT_NAME)
    private String name;

    @JsonProperty(JsonFiledName.JsonEvent.EVENT_DESCRIPTION)
    private String description;

    @JsonProperty(JsonFiledName.JsonEvent.EVENT_COUNTRY)
    private String country;

    @JsonProperty(JsonFiledName.JsonEvent.EVENT_IS_OPEN)
    private boolean isOpen;

    @JsonProperty(JsonFiledName.JsonEvent.EVENT_START_DATE)
    private Date startDate;

    @JsonProperty(JsonFiledName.JsonEvent.EVENT_END_DATE)
    private Date endDate;

    public Event(
            int idEvent,
            String name,
            String description,
            String country,
            boolean isOpen,
            Date startDate,
            Date endDate) {
        this.idEvent = idEvent;
        this.name = name;
        this.description = description;
        this.country = country;
        this.isOpen = isOpen;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCountry() {
        return country;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return idEvent == event.idEvent &&
                isOpen == event.isOpen &&
                name.equals(event.name) &&
                Objects.equals(description, event.description) &&
                Objects.equals(country, event.country) &&
                startDate.equals(event.startDate) &&
                endDate.equals(event.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEvent, name, description, country, isOpen, startDate, endDate);
    }
}
