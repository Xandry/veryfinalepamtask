package core.factory.index;

import core.factory.Director;
import core.factory.PageType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class IndexDirector implements Director {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher(PageType.getPage(PageType.INDEX)).forward(request, response);
    }
}
