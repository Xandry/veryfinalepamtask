package core.factory;

import core.factory.competitionlist.CompetitionListDirector;
import core.factory.index.IndexDirector;
import core.factory.result.ResultDirector;
import core.factory.setlocale.SetLocaleDirector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The class implements Factory pattern for {@code Director} interface.
 *
 * @see Director
 */
public class PageDirectorFactory {
    private static final Logger logger = LogManager.getLogger(PageDirectorFactory.class);

    /**
     * @param pageType page that user want to get.
     * @return {@code Director} class that will handle this request.
     * @see Director
     */
    public static Director createPageDirector(PageType pageType) {
        logger.debug("Creating Director for " + pageType + " page.");

        switch (pageType) {
            case RESULT:
                return new ResultDirector();
            case COMPETITION_LIST:
                return new CompetitionListDirector();
            case SET_LOCALE:
                return new SetLocaleDirector();
            case INDEX:
            default:
                return new IndexDirector();
        }
    }
}
