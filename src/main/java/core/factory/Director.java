package core.factory;

import data.rallydao.exception.DataCollectionDaoErrorException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class that implements the interface can handle one specific request all the way through. <br>
 * One implemented class - one request type. <br>
 * <b>{@code CABINET} and {@code LOGOUT} pages etc. aren't
 * handling by this class - check this {@link webapp.state.State}</b> <br>
 */
public interface Director {

    /**
     * This method takes information from HTTP request that comes to a servlet.
     * The method process this information, makes some request to DAO if it's necessary and
     * forward user to result .jsp page
     *
     * @param request  Request that contains required information.
     * @param response Response that will be send
     * @throws DataCollectionDaoErrorException if any error occupies in data reading process.
     */
    void handle(HttpServletRequest request, HttpServletResponse response)
            throws DataCollectionDaoErrorException, ServletException, IOException;
}
