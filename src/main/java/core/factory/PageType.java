package core.factory;


/**
 * All pages and actions of the project.
 */
public enum PageType {
    ERROR,
    SET_LOCALE,

    INDEX,
    RESULT,
    COMPETITION_LIST,

    GOTO_EVENT_CREATION,
    CREATE_EVENT,

    ALL_USERS,
    DELETE_USER,

    PARTICIPATE,
    GOTO_PARTICIPATE,

    GOTO_EDIT_RESULT,
    EDIT_RESULT,

    TEAM,
    CREATE_TEAM,
    GOTO_CREATION_TEAM,

    REGISTRATION,
    CABINET,
    ADMIN_MENU,
    USER_MENU,
    LOGOUT;


    /**
     * This method contains names of every .jsp pages of the project.
     *
     * @param pageType Type of page you want to get.
     * @return Name of the {@code .jsp} page.
     */
    public static String getPage(PageType pageType) {
        switch (pageType) {
            case RESULT:
                return "result.jsp";
            case COMPETITION_LIST:
                return "competitions.jsp";
            case ADMIN_MENU:
                return "admin_menu.jsp";
            case USER_MENU:
                return "user_menu.jsp";
            case ALL_USERS:
                return "users_editing.jsp";
            case GOTO_CREATION_TEAM:
                return "create_team.jsp";
            case GOTO_EVENT_CREATION:
                return "create_event.jsp";
            case GOTO_PARTICIPATE:
                return "participate.jsp";
            case GOTO_EDIT_RESULT:
                return "edit_result.jsp";
            case ERROR:
                return "error/error.jsp";
//            case CABINET:
            // it will be handled in AuthFilter
//            case LOGOUT:
            //it will be handled by AuthFilter
            default:
            case INDEX:
                return "index.jsp";
        }
    }
}
