package core.factory.setlocale;

import core.factory.Director;
import webapp.AttributeName;
import webapp.Language;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SetLocaleDirector implements Director {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) {
        String locale = Language.parseString(request.getParameter(AttributeName.LANGUAGE));
        request.getSession().setAttribute(AttributeName.LANGUAGE, locale);
    }
}
