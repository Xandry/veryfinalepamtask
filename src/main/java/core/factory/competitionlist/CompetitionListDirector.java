package core.factory.competitionlist;

import application.rally.Event;
import core.factory.Director;
import core.factory.PageType;
import data.rallydao.exception.DataCollectionDaoErrorException;
import data.rallydao.mysql.DatabaseRallyDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import webapp.AttributeName;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static core.factory.PageType.COMPETITION_LIST;
import static core.factory.PageType.getPage;


public class CompetitionListDirector implements Director {
    private static final Logger logger = LogManager.getLogger(CompetitionListDirector.class);

    /**
     * Page that user will be forward to by the director.
     */
    private final PageType PAGE_TYPE = COMPETITION_LIST;


    /**
     * This implemented method handles request to list of competitions.
     *
     * @param request  Request that comes to the servlet
     * @param response Response that comes to the servlet
     * @throws DataCollectionDaoErrorException if any error occupies in data reading process.
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response)
            throws DataCollectionDaoErrorException {
        logger.debug(this.getClass().getSimpleName() + " is handling the request to " + PAGE_TYPE + " : " + getPage(PAGE_TYPE));

        DatabaseRallyDao rallyDAO = new DatabaseRallyDao();
        ArrayList<Event> events = rallyDAO.getEvents();
        request.setAttribute(AttributeName.EVENTS, events);

        try {
            request.getRequestDispatcher(getPage(PAGE_TYPE)).forward(request, response);
            logger.trace(this.getClass().getSimpleName() + " has handled the request to " + PAGE_TYPE + " : " + getPage(PAGE_TYPE));
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            logger.fatal(e + " in " + this.getClass().getSimpleName());
        }
    }
}
