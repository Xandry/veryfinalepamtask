package core.factory.result;

import application.rally.Event;
import application.rally.Result;
import application.resultcomparator.ResultTimeComparator;
import core.factory.Director;
import core.factory.PageType;
import data.rallydao.RallyDao;
import data.rallydao.exception.DataCollectionDaoErrorException;
import data.rallydao.mysql.DatabaseRallyDao;
import webapp.AttributeName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static core.factory.PageType.RESULT;
import static core.factory.PageType.getPage;

public class ResultDirector implements Director {
    private static final Logger logger = LogManager.getLogger(ResultDirector.class);

    /**
     * Page that user will be forward to by the director.
     */
    private final PageType PAGE_TYPE = RESULT;

    public ResultDirector() {
    }

    /**
     * This implemented method handles request to result of a competition.
     *
     * @param request  Request that comes to the servlet
     * @param response Response that comes to the servlet
     * @throws DataCollectionDaoErrorException if any error occupies in data reading process.
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response)
            throws DataCollectionDaoErrorException {
        logger.debug(this.getClass().getSimpleName() + " is handling the request to " + PAGE_TYPE + " : " + getPage(PAGE_TYPE));
        try {
            RallyDao rallyDAO = new DatabaseRallyDao();

            int idEvent = Integer.parseInt(request.getParameter(AttributeName.ID));
            RallyDao rallyDao = new DatabaseRallyDao();
            Event event = rallyDAO.getEventById(idEvent);

            ArrayList<Result> results = rallyDAO.getResults(idEvent);
            results.sort(new ResultTimeComparator());

            request.setAttribute(AttributeName.EVENT, event);
            request.setAttribute(AttributeName.RESULTS, results);

            request.getRequestDispatcher(PageType.getPage(PAGE_TYPE)).forward(request, response);
            logger.trace(this.getClass().getSimpleName() + "is handling the request to " + PAGE_TYPE + " : " + getPage(PAGE_TYPE));
        } catch (ServletException | IOException | DataCollectionDaoErrorException e) {
            logger.error(e);
            e.printStackTrace();
            throw new DataCollectionDaoErrorException(e);
        }
    }
}